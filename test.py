
from qgraphs import *
from qgraphs.draw import *


choice = 0

if choice == 0:
    # 4 node graph with several perfect matches
    # using "string" shorthand to specify cnode,
    # e.g. "0b" --> CNode(node='0', color='b')
    edgelist = [Edge("0b","1b"),
            Edge("1b","2b"), Edge("1b","2r"), Edge("1r","2r"),
            Edge("2g","3g"), Edge("3b","0b"),
            Edge("0x","2x")]
elif choice == 1:
    # same 4 node graph, but writing cnodes as (node#,color#) instead
    edgelist = [Edge((0,0),(1,0)),
            Edge((1,0),(2,0)), Edge((1,0),(2,1)), Edge((1,1),(2,1)),
            Edge((2,2),(3,2)), Edge((3,0),(0,0)),
            Edge((0,3),(2,3))]
else:
    # test to see 12 colors
    edgelist = [Edge("00","10"), Edge("01","21"), Edge("02","32"),
        Edge("03","43"), Edge("04","54"), Edge("05","65"), Edge("06","76"), Edge("07","87"),
        Edge("08","98"), Edge("09","a9"), Edge("0x","bx"), Edge("0z","cz"),
    ]

pminfo = calculate_perfect_matching_info(edgelist)
print("state summary:", pminfo.summary)

draw_graph(edgelist)

fig = plt.gcf()
fig.savefig('out.png')
plt.show()

'''
for clist, pmlist in pminfo.components.items():
    print("clist:",clist)
    for pm in pmlist:
        print("  weight:",pm.weight())
        draw_graph(pm.edges)
        plt.show()
'''

'''
draw_graph_components(pminfo)
fig = plt.gcf()
fig.savefig('all.png')
plt.show()
'''

'''
for clist in pminfo.components:
    draw_graph_components(pminfo, only_clist=clist)
    plt.show()
'''

