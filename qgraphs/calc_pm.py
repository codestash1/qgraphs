
from collections import (defaultdict, namedtuple)


class MaskedMatchingTerm:
    """
    A 'matching' in graph theory is a set of edges without common vertices.
    This object is intended to allow simple tracking and combining matchings,
    for constructing perfect matchings.
    """

    def __init__(self, numNodes, edge=None, nodeIndex=None):
        self.n = numNodes
        self.nodemask = 0
        self.edges = []
        if edge:
            self.add_edge(edge, nodeIndex)

    def copy(self):
        out = MaskedMatchingTerm(self.n)
        out.nodemask = self.nodemask
        out.edges = list(self.edges)
        return out

    def add_edge(self, edge, nodeIndex=None):
        n1 = edge.cnodes[0].node
        n2 = edge.cnodes[1].node
        if nodeIndex:
            assert(self.n == len(nodeIndex))
            n1 = nodeIndex[n1]
            n2 = nodeIndex[n2]
        if not all(isinstance(x,int) for x in [n1,n2]):
            raise TypeError("node ids were not convertable to integers, "
                            "supply appropriate nodeIndex to correct")
        mask = ((1 << n1) | (1 << n2))
        if (mask & self.nodemask):
            raise ValueError("multiple edges incident to the same node")
        self.nodemask |= mask
        self.edges.append(edge)

    def try_combine_term(self, term):
        if (term.nodemask & self.nodemask):
            return None
        out = MaskedMatchingTerm(self.n)
        out.nodemask = (self.nodemask | term.nodemask)
        out.edges = (self.edges + term.edges)
        return out

    def weight(self):
        if len(self.edges) == 0:
            return 0
        w = self.edges[0].weight
        for edge in self.edges[1:]:
            w *= edge.weight
        return w

    def clist(self, nodeIndex=None):
        out = [None for _ in range(self.n)]
        for edge in self.edges:
            n1,c1 = edge.cnodes[0]
            n2,c2 = edge.cnodes[1]
            if nodeIndex:
                n1 = nodeIndex[n1]
                n2 = nodeIndex[n2]
            out[n1] = c1
            out[n2] = c2
        return tuple(out)

PerfectMatchingInfo = namedtuple('PerfectMatchingInfo', [
    'edgelist',
    'nodeIds','colorIds','nodeIndex','colorIndex',
    'components',  # dict clist -> list(MaskedMatchingTerm)
    'summary'
])

def calculate_perfect_matching_info(edgelist):
    # extract edgelist into more useful information
    edges = defaultdict(list)
    nodeColors = defaultdict(set)
    colorIds = set()
    nodeIds = set()
    for edge in edgelist:
        n1,c1 = edge.cnodes[0]
        n2,c2 = edge.cnodes[1]
        colorIds.add(c1)
        colorIds.add(c2)
        nodeIds.add(n1)
        nodeIds.add(n2)
        edges[(n1,n2)].append((c1,c2))
        nodeColors[n1].add(c1)
        nodeColors[n2].add(c2)
    # convert to sorted lists
    nodeIds = sorted(nodeIds)
    colorIds = sorted(colorIds)
    nodeColors = {n:sorted(clrs) for n,clrs in nodeColors.items()}
    numNodes = len(nodeIds)
    numColors = len(colorIds)

    nodeIndex = {nodeId:i for i,nodeId in enumerate(nodeIds)}
    colorIndex = {nodeId:i for i,nodeId in enumerate(nodeIds)}
    minNodeTerms = defaultdict(list)
    for edge in edgelist:
        n1 = nodeIndex[edge.cnodes[0].node]
        n2 = nodeIndex[edge.cnodes[1].node]
        minNodeTerms[min(n1,n2)].append(MaskedMatchingTerm(numNodes, edge, nodeIndex))

    # extract perfect matches,
    # this is essentially a depth first traversal
    def _recurse_collect_pm(collection, currentTerm, currentNode):
        while (currentTerm.nodemask & (1 << currentNode)):
            currentNode += 1
        if currentNode < numNodes:
            for term in minNodeTerms[currentNode]:
                term2 = currentTerm.try_combine_term(term)
                if term2:
                    _recurse_collect_pm(collection, term2, currentNode+1)
        else:
            # got a perfect matching
            collection[currentTerm.clist(nodeIndex)].append(currentTerm)

    components = defaultdict(list)  # tuple(clist) -> list(pm)
    _recurse_collect_pm(components, MaskedMatchingTerm(numNodes), 0)

    summary = []
    for clist,pmlist in components.items():
        cstr = "{" + "".join(str(x) for x in clist) + "}"
        w = sum(term.weight() for term in pmlist)
        # all this nonsense is just to format it nicely like a polynomial
        if isinstance(w,complex) and w.imag == 0:
            w = w.real
        if isinstance(w,float) and int(w) == w:
            w = int(w)
        if w == 0:
            pass
        elif w == 1:
            if len(summary):
                summary.append("+ " + cstr)
            else:
                summary.append(cstr)
        elif w == -1:
            summary.append("- " + cstr)
        elif not isinstance(w,float) and not isinstance(w,int):
            if len(summary):
                summary.append(f"+ ({w}) {cstr}")
            else:
                summary.append(f"({w}) {cstr}")
        elif w < 0:
            summary.append(str(w) + " " + cstr)
        elif len(summary):
            summary.append("+ " + str(w) + " " + cstr)
        else:
            summary.append(str(w) + " " + cstr)
    summary = " ".join(summary)

    return PerfectMatchingInfo(edgelist,
                               nodeIds, colorIds, nodeIndex, colorIndex,
                               dict(components), summary)

