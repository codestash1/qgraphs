
from collections import namedtuple


_CNode = namedtuple('_CNode', ['node','color'])
class CNode(_CNode):
    def __new__(cls, *data):
        """
        initialize as CNode(node,color) or CNode(iterable)
        examples: CNode(0,1), CNode("a1")
        """
        if len(data) == 2:
            node, color = data
        elif len(data) == 1:
            node, color = data[0]
        else:
            raise ValueError(f"invalid CNode initializer data : {data}")
        return super().__new__(cls, node, color)


_Edge = namedtuple('_Edge', ['cnodes','weight'])
class Edge(_Edge):
    def __new__(cls, cnode1, cnode2, weight=1):
        """
        cnode1, cnode2 may be CNodes or CNode initializers
        """
        if not isinstance(cnode1,CNode):
            cnode1 = CNode(cnode1)
        if not isinstance(cnode2,CNode):
            cnode2 = CNode(cnode2)
        if cnode1.node == cnode2.node:
            raise ValueError("Edge must be between distinct nodes")
        # put in sorted order
        if cnode2 < cnode1:
            cnode1, cnode2 = cnode2, cnode1
        return super().__new__(cls, (cnode1,cnode2), weight)


