
from collections import defaultdict
import math

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import (Circle, Arc, Polygon)
from matplotlib.transforms import Affine2D

__all__ = ['plt', 'draw_graph', 'rainbow_text', 'draw_graph_components']



default_colors = [
    'blue', 'red', 'green', 'magenta',
    'yellow', 'cyan', 'orange', 'purple',
    'brown', 'olive', 'pink', '#DDA0DD', '#FA8072',
]

simple_color_labels = {
    'b':'blue', 'g':'green', 'r':'red', 'c':'cyan', 'm':'magenta', 'y':'yellow',
    'o':'orange', 'p':'purple',
}


def circular_layout(nodeIds):
    n = len(nodeIds)
    x0,y0,r = 0.5, 0.5, 0.4
    phi0 = math.pi/2 + math.pi/n
    step = -(2*math.pi)/n
    direction = -1
    pos = [(x0 + r*math.cos(phi0+i*step), y0 + r*math.sin(phi0+i*step)) for i in range(n)]
    return {nodeIds[i]:np.array(pos[i]) for i in range(n)}

def circle_params_from_three_points(p1,p2,p3):
    # "Get the equation of a circle when given 3 points"
    # https://math.stackexchange.com/a/4000949
    # http://www.ambrsoft.com/TrigoCalc/Circle3D.htm
    x1, y1 = p1
    x2, y2 = p2
    x3, y3 = p3
    N1 = (x1*x1+y1*y1)
    N2 = (x2*x2+y2*y2)
    N3 = (x3*x3+y3*y3)
    A = x1*(y2 - y3) - y1*(x2 - x3) + (x2*y3 - x3*y2)
    B = N1*(y3-y2) + N2*(y1-y3) + N3*(y2-y1)
    C = N1*(x2-x3) + N2*(x3-x1) + N3*(x1-x2)
    D = N1*(x3*y2-x2*y3) + N2*(x1*y3-x3*y1) + N3*(x2*y1-x1*y2)
    xc = -B/(2*A)
    yc = -C/(2*A)
    r = math.sqrt((B*B + C*C - 4*A*D)/(4*A*A))
    return xc,yc,r



graph_default_opts = {
    "colors": None, # dict: colorId -> color, default will assign from default_colors
    "multiedge_arcs": True,  # False will use two line segments with and angle for multiedges
    "edge_linewidth": 4.0,
    "text_size": 18,
    "node_layout": "circular",
    "node_size": 0.03,
    "node_labels": True, # True will use str(nodeId), otherwise dict: nodeId -> str
    "node_edgecolor": "black",
    "node_facecolor": True, # options:
                            #   True: will use edgecolors if single color else multicnode_color,
                            #   str: matplotlib color specification
                            #   dict: nodeId -> color
    "multicnode_color": "gray",
    "node_labels_usetex": False,
    }

def draw_graph(edgelist, ax=None, **kwargs):
    opt = dict(graph_default_opts)
    opt.update(kwargs)

    if ax is None:
        fig = plt.figure(figsize=(4.0,4.0))
        # axes quantities are in fractions of figure widths and height
        #ax = fig.add_axes((0.0,0.0,1.0,1.0))
        ax = fig.add_subplot(1,1,1)
        ax.axis("off")
        ax.set_aspect('equal')

    node_default_opts = {
        "radius":opt["node_size"],
        "edgecolor":opt["node_edgecolor"],
        "fill":True,
        "zorder":2,
    }
    line_default_opts = {
        "closed":False,
        "linewidth":opt["edge_linewidth"],
        "zorder":1,
    }
    arc_default_opts = {
        "linewidth":opt["edge_linewidth"],
        "zorder":1,
    }

    # for debugging purposes, allow defaults to be overridden
    def _node(pos, **kwargs):
        opts = dict(node_default_opts)
        opts.update(kwargs)
        return Circle(pos, **opts)

    def _line(points, **kwargs):
        opts = dict(line_default_opts)
        opts.update(kwargs)
        return Polygon(np.array(points), **opts)

    def _circular_arc(center, r, theta1, theta2, **kwargs):
        opts = dict(arc_default_opts)
        opts.update(kwargs)
        if theta1 > theta2:
            theta1, theta2 = theta2, theta1
        return Arc(np.array(center), 2*r, 2*r, 0.0, theta1, theta2, **opts)

    # extract edgelist into more useful information
    edges = defaultdict(list)
    nodeColors = defaultdict(set)
    colorIds = set()
    nodeIds = set()
    for edge in edgelist:
        n1,c1 = edge.cnodes[0]
        n2,c2 = edge.cnodes[1]
        colorIds.add(c1)
        colorIds.add(c2)
        nodeIds.add(n1)
        nodeIds.add(n2)
        edges[(n1,n2)].append((c1,c2))
        nodeColors[n1].add(c1)
        nodeColors[n2].add(c2)
    # convert to sorted lists
    nodeIds = sorted(nodeIds)
    colorIds = sorted(colorIds)
    nodeColors = {n:sorted(clrs) for n,clrs in nodeColors.items()}

    if opt["node_layout"] == "circular":
        pos = circular_layout(nodeIds)
    elif isinstance(opt["node_layout"], dict):
        pos = opts["node_layout"]
    else:
        raise ValueError("invalid node_layout : {}".format(opt["node_layout"]))

    if opt["colors"] is None:
        available = default_colors[::-1]
        colors = {}
        for colorId in colorIds:
            if colorId in simple_color_labels:
                c = simple_color_labels[colorId]
                colors[colorId] = c
                available.remove(c)
        for colorId in colorIds:
            if colorId not in colors:
                colors[colorId] = available.pop()
    elif isinstance(opt["colors"], dict):
        colors = opt["colors"]
    else:
        raise ValueError("invalid colors : {}".format(opt["colors"]))

    if opt["node_labels"] in [None, False]:
        node_labels = None
    elif opt["node_labels"] == True:
        node_labels = {nodeId:str(nodeId) for nodeId in nodeIds}
    elif isinstance(opt["node_labels"],dict):
        node_labels = opt["node_labels"]
    else:
        raise ValueError("invalid node_labels : {}".format(opt["node_labels"]))

    if opt["node_facecolor"] == True:
        node_color = {}
        for n in nodeIds:
            clrs = nodeColors[n]
            if len(clrs) == 1:
                node_color[n] = colors[clrs[0]]
            else:
                node_color[n] = opt["multicnode_color"]
    elif isinstance(opt["node_facecolor"],str):
        node_color = {n:opt["node_facecolor"] for n in nodeIds}
    elif isinstance(opt["node_facecolor"],dict):
        node_color = opt["node_facecolor"]
    else:
        raise ValueError("invalid node_facecolor : {}".format(opt["node_facecolor"]))

    for n in nodeIds:
        if 0:
            # drawing node and text separately makes it difficult to size
            ax.add_patch(_node(pos[n], facecolor=node_color[n]))
            x,y = pos[n]
            ax.text(x, y, node_labels[n], ha='center', va='center', zorder=3)
        elif node_labels:
            x,y = pos[n]
            ax.text(x, y, node_labels[n],
                    size = opt["text_size"],
                    bbox=dict(
                            boxstyle="circle,pad=0.3",
                            lw=1,
                            edgecolor=opt["node_edgecolor"],
                            facecolor=node_color[n]
                            ),
                    ha='center', va='center',
                    usetex=opt["node_labels_usetex"], zorder=2)
        else:
            ax.add_patch(_node(pos[n]))

    for nodepair,edgecolors in edges.items():
        n1,n2 = nodepair
        if len(edgecolors) == 1:
            c1,c2 = edgecolors[0]
            mid = (pos[n1] + pos[n2])/2
            ax.add_patch(_line([pos[n1], mid], edgecolor=colors[c1]))
            ax.add_patch(_line([mid, pos[n2]], edgecolor=colors[c2]))
        else:
            mid = (pos[n1] + pos[n2])/2
            vx,vy = pos[n2] - pos[n1]
            perp = np.array([vy,-vx])*0.10
            offset = -(len(edgecolors)-1)/2
            for i,colorpair in enumerate(edgecolors):
                c1,c2 = colorpair
                mid2 = mid+(i+offset)*perp
                #arc = patches.Arc(center, weight, hight, angle, theta1, theta2)
                if not opt["multiedge_arcs"] or (i+offset) == 0:
                    ax.add_patch(_line([pos[n1], mid2], edgecolor=colors[c1]))
                    ax.add_patch(_line([mid2, pos[n2]], edgecolor=colors[c2]))
                else:
                    x1,y1 = pos[n1]
                    x2,y2 = pos[n2]
                    xc,yc,r = circle_params_from_three_points(pos[n1],mid2,pos[n2])
                    #print("{} -> {}".format((pos[n1],mid2,pos[n2]),(xc,yc,r)))
                    theta1 = math.atan2(y1-yc,x1-xc)*180/math.pi
                    theta2 = math.atan2(y2-yc,x2-xc)*180/math.pi
                    if abs(theta1 - theta2) > 180:
                        if theta1 < 0:
                            theta1 += 360
                        if theta2 < 0:
                            theta2 += 360
                    tmid = (theta1+theta2)/2
                    #print("theta1={} theta2={}".format(theta1,theta2))
                    ax.add_patch(_circular_arc((xc,yc), r, theta1, tmid, edgecolor=colors[c1]))
                    ax.add_patch(_circular_arc((xc,yc), r, tmid, theta2, edgecolor=colors[c2]))


# https://matplotlib.org/stable/gallery/text_labels_and_annotations/rainbow_text.html
def rainbow_text(x, y, strings, colors, orientation='horizontal',
                 ax=None, **kwargs):
    """
    Take a list of *strings* and *colors* and place them next to each
    other, with text strings[i] being shown in colors[i].

    Parameters
    ----------
    x, y : float
        Text position in data coordinates.
    strings : list of str
        The strings to draw.
    colors : list of color
        The colors to use.
    orientation : {'horizontal', 'vertical'}
    ax : Axes, optional
        The Axes to draw into. If None, the current axes will be used.
    **kwargs
        All other keyword arguments are passed to plt.text(), so you can
        set the font size, family, etc.
    """
    if ax is None:
        ax = plt.gca()
    t = ax.transData
    canvas = ax.figure.canvas

    assert orientation in ['horizontal', 'vertical']
    if orientation == 'vertical':
        kwargs.update(rotation=90, verticalalignment='bottom')

    for s, c in zip(strings, colors):
        text = ax.text(x, y, s + " ", color=c, transform=t, **kwargs)

        # Need to draw to update the text position.
        text.draw(canvas.get_renderer())
        ex = text.get_window_extent()
        if orientation == 'horizontal':
            t = text.get_transform() + Affine2D().translate(ex.width, 0)
        else:
            t = text.get_transform() + Affine2D().translate(0, ex.height)


def draw_graph_components(pminfo, only_clist=None, fig=None, **kwargs):
    #opt = dict(graph_default_opts)
    #opt.update(kwargs)

    numNodes = len(pminfo.nodeIds)
    if only_clist:
        clists = [only_clist]
        yoffset = 0
    else:
        clists = [clist for clist in pminfo.components]
        yoffset = 1
    componentLengths = [len(pminfo.components[clist]) for clist in clists]
    ytiles = len(clists) + yoffset
    xtiles = 1 + max(componentLengths)

    if fig is None:
        fig = plt.figure(figsize=(4.0*xtiles,4.0*ytiles))

    axes = fig.subplots(nrows=ytiles, ncols=xtiles)
    for ax in axes.flatten():
        ax.axis("off")
        ax.set_aspect('equal')

    # simplify handling by always treating this as a 2D array
    if ytiles == 1:
        axes = np.array([axes])

    # draw full graph
    if yoffset:
        ax = axes[0,0]
        draw_graph(pminfo.edgelist, ax=ax, **kwargs)

    # draw components for each clist
    solid_block = "\u25A0"
    for y in range(yoffset,ytiles):
        clist = clists[y-yoffset]
        pmlist = pminfo.components[clist]

        # draw total component weight
        ax = axes[y,0]
        total_weight = sum(term.weight() for term in pmlist)
        if total_weight > 0:
            wstr = "+" + str(total_weight)
        else:
            wstr = str(total_weight)

        rainbow_text(0.0, 0.5,
            [wstr + " {"] + [solid_block]*numNodes + ["}"],
            ["k"] + [str(x) for x in clist] + ["k"],
            ax=ax, size=10)

        # draw individual pm
        for i,term in enumerate(pmlist):
            ax = axes[y,i+1]
            draw_graph(term.edges, ax=ax, **kwargs)

            w = term.weight()
            if w > 0:
                wstr = "+" + str(w)
            else:
                wstr = str(w)
            wstr = "weight: " + wstr
            ax.text(0.1, 0.0, wstr, color="k", size=8)


