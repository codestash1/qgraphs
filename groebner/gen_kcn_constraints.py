#!/usr/bin/env python3

import itertools
import sys

sys.path.append("..")
from qgraphs import *
from qgraphs.draw import *

if len(sys.argv) != 4:
    print("Usage: gen_kcn_constraints.py k c n")
    sys.exit(1)

k = int(sys.argv[1])
numColors = int(sys.argv[2])
numNodes = int(sys.argv[3])


def edge_name(edge):
    n1,c1 = edge.cnodes[0]
    n2,c2 = edge.cnodes[1]
    return "w{}{}{}{}".format(str(n1),str(n2),str(c1),str(c2))

def clist_name(clist):
    return "p" + "".join(str(c) for c in clist)


# lazy "tee" print
f = open(f"full_k{k}c{numColors}n{numNodes}.s","w")
org_print = print
def print(s):
    org_print(s)
    f.write(s+"\n")


clists = []
for c in range(numColors):
    clists.append([c]*k + [0]*(numNodes-k))
clists = tuple(tuple(clist) for clist in clists)

multiedges = []
edgeset = set()
for n1 in range(numNodes):
    if n1 < k:
        color_opts1 = list(range(numColors))
    else:
        color_opts1 = [0]
    for n2 in range(n1+1,numNodes):
        if n2 < k:
            color_opts2 = list(range(numColors))
        else:
            color_opts2 = [0]
        nodepair_edgeset = set()
        for c1 in color_opts1:
            for c2 in color_opts2:
                edge = Edge((n1,c1),(n2,c2))
                edgeset.add(edge)
                nodepair_edgeset.add(edge)
        multiedges.append(sorted(nodepair_edgeset))
edgelist = sorted(edgeset)

pminfo = calculate_perfect_matching_info(edgelist)

vlist = [edge_name(edge) for edge in edgelist]

print("// k={} n={} c={}".format(k, numNodes, numColors))
print("// num variables: {}".format(len(vlist)))
group = numColors*numColors
pieces = [", ".join(vlist[x:x+group]) for x in range(0,len(vlist),group)]
print("ring R = QQ[\n    " + ",\n    ".join(pieces) + "];")

constraint_clists = [clist for clist in pminfo.components]
constraint_clists.sort()
plist = []
for clist in constraint_clists:
    pname = clist_name(clist)
    if clist in clists:
        plist.append("{} - 1".format(pname))
    else:
        plist.append("{}".format(pname))
    pmlist = pminfo.components[clist]
    tlist = []
    for term in pmlist:
        tlist.append("*".join(edge_name(edge) for edge in term.edges))
    print("poly " + pname + "=\n    " + "\n  + ".join(tlist) + ";")
print("ideal I=\n  " + ",\n  ".join(plist) + ";")
print("""option(redSB);
option(prot);
ideal I2 = slimgb(I);
I2;""")

f.close()

