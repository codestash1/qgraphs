
import sys
import itertools



def edgeWeightName(n1,n2,c1,c2):
    if n1 < n2:
        return f"w{n1}{n2}{c1}{c2}"
    return f"w{n2}{n1}{c2}{c1}"

def vcWeightName(clist):
    """name for vertex coloring weight as a polynomial of edge weights"""
    return "p" + "".join(str(c) for c in clist)



def perfectMatchings(n):
    """
    calculate all perfect matchings on K_n
    returns list of matchings: list of edges: tuple of node numbers
    """
    if n%2 != 0:
        raise ValueError("number of nodes should be even")
    return _perfectMatchings(range(n))

def _perfectMatchings(nodelist):
    """recursively find perfect matchings on remaining nodes"""
    # base case, only two nodes left
    if len(nodelist) == 2:
        return [[tuple(nodelist),],]

    matchings = []
    # loop over ways to match first node with other nodes
    for n in nodelist[1:]:
        mStart = [(nodelist[0],n),]
        # get list of what nodes are left to match
        nextlist = list(nodelist[1:])
        nextlist.remove(n)
        # recursively find all the matchings of remaining nodes
        # and combine with our starting match choice
        for mSubgraph in _perfectMatchings(nextlist):
            mCombined = list(mStart)
            mCombined.extend(mSubgraph)
            matchings.append(mCombined)
    return matchings


def define_ring(ringName, numNodes, numColors):
    vlist = []
    for n1 in range(numNodes):
        for n2 in range(n1+1,numNodes):
            for c1 in range(numColors):
                for c2 in range(numColors):
                    vlist.append(edgeWeightName(n1,n2,c1,c2))
    group = numColors*numColors
    pieces = [", ".join(vlist[x:x+group]) for x in range(0,len(vlist),group)]
    return f"ring {ringName} = QQ[\n    " + ",\n    ".join(pieces) + "];"

def _matchingTerm(matching, colorList):
    vlist = []
    for n1,n2 in matching:
        vlist.append(edgeWeightName(n1, n2, colorList[n1], colorList[n2]))
    return "*".join(vlist)

def define_vc_poly(matchingList, colorList):
    name = vcWeightName(colorList)
    tlist = []
    for m in matchingList:
        tlist.append(_matchingTerm(m, colorList))
    return f"poly {name} = " + " + ".join(tlist) + ";"

def vcConstraintStr(clist):
    pname = vcWeightName(clist)
    if all(c==clist[0] for c in clist):
        return f"{pname} - 1"
    return f"{pname}"


def make_base_file(fname, numNodes, numColors):
    matchingList = perfectMatchings(numNodes)
    with open(fname,"w") as f:
        f.write(f"// n={numNodes} c={numColors}\n")
        nvar = (numNodes*(numNodes-1)//2)*numColors*numColors
        f.write(f"// total variables: {nvar}\n")
        constraints = [vcConstraintStr(clist) for clist in 
                       itertools.product(range(numColors),repeat=numNodes)]
        f.write(f"// total vertex colorings: {len(constraints)}\n")
        f.write(f"int svecho=echo; echo=0;\n");
        f.write(define_ring("R", numNodes, numColors) + "\n")
        for clist in itertools.product(range(numColors),repeat=numNodes):
            f.write(define_vc_poly(matchingList, clist) + "\n")

        # ideal of monochromatic constraints
        f.write("ideal I0 =\n  " + ",\n  ".join(constraints) + ";\n")

        # ideal of monomials we can derive from I0

        f.write("ideal proof2n1 =\n")
        tlist = []
        for n1 in range(numNodes):
            othernodes = tuple(n for n in range(numNodes) if n!=n1)
            for c1 in range(numColors):
                othercolors = tuple(c for c in range(numColors) if c!=c1)
                choices = [othercolors]*numNodes
                choices[n1] = (c1,)
                for clist in itertools.product(*choices):
                    vlist = []
                    for n2 in othernodes:
                        vlist.append(edgeWeightName(n1, n2, c1, clist[n2]))
                    tlist.append("*".join(vlist))
        f.write("  " + ",\n  ".join(tlist) + ";\n")

        f.write("ideal proof2n =\n")
        tlist = []
        for n1 in range(numNodes):
            othernodes = tuple(n for n in range(numNodes) if n!=n1)
            for c1 in range(numColors):
                othercolors = tuple(c for c in range(numColors) if c!=c1)
                choices = [othercolors]*numNodes
                for clist in itertools.product(*choices):
                    if all(c==clist[0] for c in clist):
                        continue
                    vlist = []
                    for n2 in othernodes:
                        vlist.append(edgeWeightName(n1, n2, clist[n1], clist[n2]))
                    tlist.append("*".join(vlist))
        f.write("  " + ",\n  ".join(tlist) + ";\n")

        f.write("ideal proofDegn2 =\n")
        tlist = []
        for n1 in range(numNodes):
            othernodes = tuple(n for n in range(numNodes) if n!=n1)
            for c1,c2 in itertools.product(range(numColors),repeat=2):
                if c1==c2:
                    continue
                for remove in othernodes:
                    nodechoices = tuple(n for n in othernodes if n!=remove)
                    vlist = []
                    for n2 in nodechoices:
                        vlist.append(edgeWeightName(n1, n2, c1, c2))
                    tlist.append("*".join(vlist))
        f.write("  " + ",\n  ".join(tlist) + ";\n")

        f.write("ideal proofMono =\n")
        tlist = []
        for c in range(numColors):
            for n1 in range(numNodes):
                othernodes = tuple(n for n in range(numNodes) if n!=n1)
                vlist = []
                for n2 in othernodes:
                    vlist.append(edgeWeightName(n1, n2, c, c))
                tlist.append("*".join(vlist))
        f.write("  " + ",\n  ".join(tlist) + ";\n")

        f.write(f"echo=svecho;\n");


def proof_2n1(node, clist_start, clist_end):
    """
    combines 2^(n-1) constraints

    clist_start and clist_end must agree on entry #node, and no others
    """
    numNodes = len(clist_start)
    assert(len(clist_end) == numNodes)
    clists = [clist_start, clist_end]
    choicelist = []
    for i in range(numNodes):
        if i==node:
            assert(clist_start[i] == clist_end[i])
            choicelist.append((0,))
        else:
            assert(clist_start[i] != clist_end[i])
            choicelist.append((0,1))
    nodelist = list(range(numNodes))
    nodelist.remove(node)

    parts = []
    for choice in itertools.product(*choicelist):
        vlist = []
        clist = [clists[opt][n] for n,opt in enumerate(choice)] # clist from choice
        for node2 in nodelist:
            c2 = clists[1-choice[node2]][node2] # clist from "opposite" choice
            vlist.append(edgeWeightName(node, node2, clist_start[node], c2))
        if sum(choice)%2:
            sign = "-"
        else:
            sign = ""
        parts.append("(" + vcConstraintStr(clist) + ")*(" + sign + "*".join(vlist) + ")")
    return "\n   +".join(parts)

def make_proof_2n1(fname, base, node, clist_start, clist_end):
    with open(fname,"w") as f:
        f.write(f"// node={node} clist_start={clist_start} clist_end={clist_end}\n")
        if base:
            f.write(f'< "{base}";\n')
        f.write("poly result =\n")
        f.write("    "  + proof_2n1(node, clist_start, clist_end) + ";\n")
        f.write("result;\n")

def proof_2n(node, clist_start, clist_end):
    """
    combines 2^n constraints

    clist_start and clist_end must differ on all entries
    """
    numNodes = len(clist_start)
    assert(len(clist_end) == numNodes)
    assert(all(clist_start[i] != clist_end[i] for i in range(numNodes)))
    clists = [clist_start, clist_end]
    choicelist = [(0,1)]*numNodes
    nodelist = list(range(numNodes))
    nodelist.remove(node)

    parts = []
    for choice in itertools.product(*choicelist):
        clist = [clists[opt][n] for n,opt in enumerate(choice)] # clist from choice
        inv_clist = [clists[1-opt][n] for n,opt in enumerate(choice)] # "opposite" clist
        if choice[node] == 0:
            vlist = []
            c1 = inv_clist[node]
            for node2 in nodelist:
                c2 = inv_clist[node2]
                vlist.append(edgeWeightName(node, node2, c1, c2))
            if sum(choice)%2:
                sign = "-"
            else:
                sign = ""
            coeff = sign + "*".join(vlist)
        else:
            tlist = []
            for node_inv in nodelist:
                vlist = []
                for node2 in nodelist:
                    c2 = inv_clist[node2]
                    if node2 == node_inv:
                        c1 = inv_clist[node]
                    else:
                        c1 = clist[node]
                    vlist.append(edgeWeightName(node, node2, c1, c2))
                tlist.append("*".join(vlist))
            if sum(choice)%2:
                start = "\n        "
                sep   = "\n       +"
            else:
                start = "\n       -"
                sep   = "\n       -"
            coeff = start + sep.join(tlist)
        parts.append("({})*({})".format(vcConstraintStr(clist), coeff))
    return "\n   +".join(parts)

def make_proof_2n(fname, base, node, clist_start, clist_end):
    with open(fname,"w") as f:
        f.write(f"// node={node} clist_start={clist_start} clist_end={clist_end}\n")
        if base:
            f.write(f'< "{base}";\n')
        f.write("poly result =\n")
        f.write("    "  + proof_2n(node, clist_start, clist_end) + ";\n")
        f.write("result;\n")

numNodes = 6
numColors = 3

base = f"base_n{numNodes}c{numColors}.s"
make_base_file(base, numNodes, numColors)
make_proof_2n1("test_2n1.s", base, 0, [0]*numNodes, [0] + [1]*(numNodes-1))
make_proof_2n("test_2n.s", base, 0, [0]*numNodes, [1]*(numNodes//2) + [2]*(numNodes//2))

# match example hand extracted from g43 calculation
#make_proof_2n("test_2n.s", base, 3, [0,0,0,0], [2,1,2,1])


