// n=6 c=3
< "base_n6c3.s";
poly result =
     w0201*w0301*w0401*w0501 *(p000000-1)
    -w0201*w0301*w0401*w0500 *p000001
    -w0201*w0301*w0400*w0501 *p000010
    +w0201*w0301*w0400*w0500 *p000011
    -w0201*w0300*w0401*w0501 *p000100
    +w0201*w0300*w0401*w0500 *p000101
    +w0201*w0300*w0400*w0501 *p000110
    -w0201*w0300*w0400*w0500 *p000111
    -w0200*w0301*w0401*w0501 *p001000
    +w0200*w0301*w0401*w0500 *p001001
    +w0200*w0301*w0400*w0501 *p001010
    -w0200*w0301*w0400*w0500 *p001011
    +w0200*w0300*w0401*w0501 *p001100
    -w0200*w0300*w0401*w0500 *p001101
    -w0200*w0300*w0400*w0501 *p001110
    +w0200*w0300*w0400*w0500 *p001111;
result;
ideal tmp = proof2n1, proof2n;
//tmp;
ideal helpI = std(tmp);
//helpI;
poly reduced_result = reduce(result, helpI);
reduced_result;

poly subgraph_xx1111 =
    w2311*w4511 + w2411*w3511 + w2511*w3411;
poly expected =
    w0100*w0200*w0300*w0400*w0500*subgraph_xx1111
   -w0201*w0301*w0401*w0501;
reduced_result-expected;

reduce(result*w0201, helpI);

poly mono_reduced = reduce(result*w0100, helpI);
mono_reduced;
poly mono_expected = w0100^2*w0200*w0300*w0400*w0500*subgraph_xx1111;
mono_reduced - mono_expected;

