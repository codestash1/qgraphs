// node=0 clist_start=[0, 0, 0, 0] clist_end=[1, 2, 2, 2]
< "base_n4c3.s";
poly result =
    w0201*w0301*(
     w0201*w0301 *(p0000-1)
    -w0201*w0300 *p0001
    -w0200*w0301 *p0010
    +w0200*w0300 *p0011
    );
result;
ideal tmp = proof2n1, proof2n;
//tmp;
ideal helpI = std(tmp);
//helpI;
reduce(result, helpI);
