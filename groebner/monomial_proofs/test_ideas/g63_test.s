// n=6 c=3
< "base_n6c3.s";
ideal I = I0, proof2n1, proof2n,
    w0100 - 1,
    w2300 - 1,
    w1211 - 1,
    w3411 - 1;
option(redSB);
option(prot);
ideal I2 = slimgb(I);
I2;
