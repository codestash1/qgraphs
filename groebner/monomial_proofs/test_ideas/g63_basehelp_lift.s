// n=6 c=3
< "base_n6c3.s";
ideal I =
  p000000 - 1,
  p000001,
  p000010,
  p000011,
  p000100,
  p000101,
  p000110,
  p000111,
  p001000,
  p001001,
  p001010,
  p001011,
  p001100,
  p001101,
  p001110,
  p001111,
  p010000,
  p010001,
  p010010,
  p010011,
  p010100,
  p010101,
  p010110,
  p010111,
  p011000,
  p011001,
  p011010,
  p011011,
  p011100,
  p011101,
  p011110,
  p011111,
    proof2n1, proof2n, proofDegn2, proofMono;

option(redSB);
option(prot);
#ideal I2 = slimgb(I);
#I2;
I;
ideal subI = 
     w0201^2*w0401*w0501*w1311
    +w0201^2*w0301*w0501*w1411
    +w0201^2*w0301*w0401*w1511
    +w0101*w0201^2*w0501*w3411
    +w0101*w0201^2*w0401*w3511
    +w0101*w0201^2*w0301*w4511;
matrix T;
matrix T2 = lift(I, subI, T, "slimgb");
T;
T2;
matrix(I)*T2;
