// n=10 c=3
ring R = QQ[
    w01rr, w02gg, w09br,
    w12bb, w16gr, w17gr,
    w26rr, w27rr, w29rr,
    w34bb, w35gg, w39rr,
    w45rr, w46gr, w47gr,
    w56br, w57br,
    w68rr,
    w78rr,
    w89rr];

poly prrrbbbrrrr=
    w01rr*w26rr*w34bb*w57br*w89rr
  + w01rr*w27rr*w34bb*w56br*w89rr
  + w01rr*w29rr*w34bb*w56br*w78rr
  + w01rr*w29rr*w34bb*w57br*w68rr;
poly prrrgggrrrr=
    w01rr*w26rr*w35gg*w47gr*w89rr
  + w01rr*w27rr*w35gg*w46gr*w89rr
  + w01rr*w29rr*w35gg*w46gr*w78rr
  + w01rr*w29rr*w35gg*w47gr*w68rr;
poly prrrrrrrrrr=
    w01rr*w26rr*w39rr*w45rr*w78rr
  + w01rr*w27rr*w39rr*w45rr*w68rr;
poly pgggbbbrrrr=
    w02gg*w16gr*w34bb*w57br*w89rr
  + w02gg*w17gr*w34bb*w56br*w89rr;
poly pggggggrrrr=
    w02gg*w16gr*w35gg*w47gr*w89rr
  + w02gg*w17gr*w35gg*w46gr*w89rr;
poly pgggrrrrrrr=
    w02gg*w16gr*w39rr*w45rr*w78rr
  + w02gg*w17gr*w39rr*w45rr*w68rr;
poly pbbbbbbrrrr=
    w09br*w12bb*w34bb*w56br*w78rr
  + w09br*w12bb*w34bb*w57br*w68rr;
poly pbbbgggrrrr=
    w09br*w12bb*w35gg*w46gr*w78rr
  + w09br*w12bb*w35gg*w47gr*w68rr;

/*
I2[1]=w29rr*w78rr+w27rr*w89rr
I2[2]=w57br*w68rr-w56br*w78rr
I2[3]=w47gr*w68rr+w46gr*w78rr
I2[4]=w29rr*w68rr+w26rr*w89rr
I2[5]=w27rr*w68rr-w26rr*w78rr
I2[6]=w17gr*w68rr+w16gr*w78rr
I2[7]=w47gr*w56br+w46gr*w57br
I2[8]=w27rr*w56br-w26rr*w57br
I2[9]=w17gr*w56br+w16gr*w57br
I2[10]=w27rr*w46gr+w26rr*w47gr
I2[11]=w17gr*w46gr-w16gr*w47gr
I2[12]=w17gr*w26rr+w16gr*w27rr
I2[13]=w01rr*w27rr*w39rr*w45rr-w09br*w12bb*w34bb*w57br
I2[14]=w01rr*w26rr*w39rr*w45rr-w09br*w12bb*w34bb*w56br
I2[15]=2*w02gg*w16gr*w35gg*w47gr*w89rr-1
I2[16]=w09br*w12bb*w34bb*w57br*w78rr+w02gg*w17gr*w35gg*w47gr*w89rr
I2[17]=2*w09br*w12bb*w34bb*w56br*w78rr-1
I2[18]=w09br*w12bb*w34bb*w56br*w68rr+w02gg*w16gr*w35gg*w46gr*w89rr
I2[19]=w02gg*w17gr*w29rr*w35gg*w47gr-w09br*w12bb*w27rr*w34bb*w57br
I2[20]=w02gg*w16gr*w29rr*w35gg*w47gr+w09br*w12bb*w26rr*w34bb*w57br
I2[21]=w02gg*w16gr*w29rr*w35gg*w46gr-w09br*w12bb*w26rr*w34bb*w56br
I2[22]=2*w02gg*w16gr*w35gg*w46gr*w78rr*w89rr+w68rr
I2[23]=2*w02gg*w16gr*w35gg*w46gr*w57br*w89rr+w56br
I2[24]=2*w09br*w12bb*w26rr*w34bb*w57br*w89rr+w29rr
I2[25]=2*w09br*w12bb*w16gr*w27rr*w34bb*w57br*w89rr-w17gr*w29rr
I2[26]=2*w09br^2*w12bb^2*w34bb^2*w56br*w57br*w89rr+w01rr*w29rr*w39rr*w45rr
I2[27]=2*w09br^2*w12bb^2*w34bb^2*w46gr*w57br^2*w89rr-w01rr*w29rr*w39rr*w45rr*w47gr
I2[28]=2*w09br^2*w12bb^2*w16gr*w34bb^2*w57br^2*w89rr-w01rr*w17gr*w29rr*w39rr*w45rr
*/

ideal I =
    // any edge set to zero causes a contradiction
    w68rr - 1,
    w56br - 1,
    w29rr - 1,
    w78rr + 1,
    w27rr - 1,
    w89rr - 1,
    w47gr - 1,
    w17gr - 1,

    prrrrrrrrrr - 2,
    pggggggrrrr - 2,
    pbbbbbbrrrr - 2,
    prrrbbbrrrr,
    prrrgggrrrr,
    pgggbbbrrrr,
    pgggrrrrrrr,
    pbbbgggrrrr;

option(redSB);
option(prot);
ideal I2 = slimgb(I);
I2;
