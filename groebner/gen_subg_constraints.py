#!/usr/bin/env python3

import sys
import argparse
import itertools

parser = argparse.ArgumentParser(description=(
    "Generates a Singular script for a Groebner basis calculation on the "
    "constraints for edge weights and subgraph weights in a monochromatic "
    "graph with numNodes and numColors. "
    "The weight of a vertex colored graph is broken into terms involving "
    "an edge weight with node 0, and the weight of the remaining subgraph."
    "This script allows odd nNodes as a generalization, even though the "
    "weight of a subgraph with an odd number of nodes does not mean "
    "anything useful in the original problem."
))
parser.add_argument("numNodes", type=int)
parser.add_argument("numColors", type=int)
group = parser.add_mutually_exclusive_group()
group.add_argument("--monochrome-edge-only","-m",
                   help="assume any multi-color edge weights are zero",
                   action="store_true")
group.add_argument("--include-all-edges","-a",
                   help="include all edge variables and include subgraph "
                        "expansions",
                   action="store_true")
if len(sys.argv) < 2:
    parser.print_help()
    sys.exit(0)
options = parser.parse_args()
numNodes = options.numNodes
numColors = options.numColors
checkSGColorCounts = (options.monochrome_edge_only and numNodes%2==0)


def perfectMatchings(nodelist):
    """
    calculate all perfect matchings on nodelist
    raises exception if nodelist has an odd length
    returns list of matchings: list of edges: tuple of node numbers
    """
    if len(nodelist)%2 != 0:
        raise ValueError("number of nodes should be even")
    matchings = _perfectMatchings(nodelist)
    matchings.sort()
    return matchings

def _perfectMatchings(nodelist):
    """recursively find perfect matchings on remaining nodes"""
    # base case, only two nodes left
    if len(nodelist) == 2:
        return [[tuple(nodelist),],]

    matchings = []
    # loop over ways to match first node with other nodes
    for n in nodelist[1:]:
        mStart = [(nodelist[0],n),]
        # get list of what nodes are left to match
        nextlist = list(nodelist[1:])
        nextlist.remove(n)
        # recursively find all the matchings of remaining nodes
        # and combine with our starting match choice
        for mSubgraph in _perfectMatchings(nextlist):
            mCombined = list(mStart)
            mCombined.extend(mSubgraph)
            matchings.append(mCombined)
    return matchings





def edgeWeightName(n1, n2, c1, c2):
    return "w{}{}{}{}".format(n1,n2,c1,c2)

def subGraphWeightName(n1, n2, clist):
    """input = two excluded nodes, and vertex coloring for full graph"""
    textlist = [str(c) for c in clist]
    textlist[n1] = 'x'
    textlist[n2] = 'x'
    return "sg" + "".join(textlist)

def checkSubgraphMonoCompatible(n1, n2, clist):
    clist2 = [c for i,c in enumerate(clist) if (i!=n1 and i!=n2)]
    if any(clist2.count(c)%2 for c in range(numColors)):
        return False
    return True

def allVariables():
    vnames = []
    for n1 in range(numNodes):
        if not options.include_all_edges and n1>0:
            break
        for n2 in range(n1+1, numNodes):
            for c1 in range(numColors):
                if options.monochrome_edge_only:
                    vnames.append(edgeWeightName(n1,n2,c1,c1))
                    continue
                for c2 in range(numColors):
                    vnames.append(edgeWeightName(n1,n2,c1,c2))
    # lazy iteration, just discard duplicates
    sgnames = set()
    n1 = 0
    for clist in itertools.product(range(numColors), repeat=numNodes):
        for n2 in range(1, numNodes):
            if checkSGColorCounts:
                if not checkSubgraphMonoCompatible(n1, n2, clist):
                    continue
            sgnames.add(subGraphWeightName(n1,n2,clist))
    sgnames = list(sgnames)
    sgnames.sort()
    vnames.extend(sgnames)
    return vnames

def constraintTerm(n1, n2, colorList):
    if options.monochrome_edge_only:
        if colorList[n1] != colorList[n2]:
            return None
        if checkSGColorCounts:
            if not checkSubgraphMonoCompatible(n1, n2, colorList):
                return None
    return "{}*{}".format(edgeWeightName(n1,n2,colorList[n1],colorList[n2]),
                          subGraphWeightName(n1,n2,colorList))

def colorConstraint(colorList):
    tlist = []
    n1 = 0
    for n2 in range(1, numNodes):
        term = constraintTerm(n1, n2, colorList)
        if term:
            tlist.append(term)
    poly = " + ".join(tlist)
    if all(c == colorList[0] for c in colorList):
        poly += " - 1"
    return poly

def allConstraints():
    plist = []
    for clist in itertools.product(range(numColors), repeat=numNodes):
        poly = colorConstraint(clist)
        if poly:
            plist.append(poly)
    if options.include_all_edges:
        for clist in itertools.product(range(numColors), repeat=numNodes):
            for n2 in range(1,numNodes):
                if clist[n2]!=0:
                    continue
                sg = subGraphWeightName(0,n2,clist)
                nlist = list(range(1,numNodes))
                nlist.remove(n2)
                tlist = []
                for matching in perfectMatchings(nlist):
                    vlist = []
                    for v1,v2 in matching:
                        c1,c2 = clist[v1], clist[v2]
                        vlist.append(edgeWeightName(v1,v2,c1,c2))
                    tlist.append("*".join(vlist))
                poly = "{} - {}".format(sg, " - ".join(tlist))
                plist.append(poly)
    return plist


vlist = allVariables() 
plist = allConstraints()

optstring = "n={} c={}".format(numNodes, numColors)
if options.monochrome_edge_only:
    optstring += " monochrome-edge-only"
print("//", optstring)
print("// total variables:", len(vlist))
print("// total constraints:", len(plist))

group = numColors*numColors
pieces = [", ".join(vlist[x:x+group]) for x in range(0,len(vlist),group)]
print("ring R = QQ[\n    " + ",\n    ".join(pieces) + "];")
print("ideal I =\n  " + ",\n  ".join(plist) + ";")
print("option(redSB);\n"
      "option(prot);\n"
      "ideal I2 = slimgb(I);\n"
      "I2;")

