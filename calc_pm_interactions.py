
from qgraphs import *
from qgraphs.draw import *


def _pm_iterator(nodePairs, numNodes, nodeMask, node):
    #print(nodePairs, numNodes, nodeMask, node)
    while (nodeMask & (1 << node)):
        node += 1
    if node >= numNodes:
        yield tuple(nodePairs)
        return
    nodeMask |= (1 << node)
    for n2 in range(node + 1, numNodes):
        if (nodeMask & (1 << n2)):
            continue
        nodePairs.append((node,n2))
        for result in _pm_iterator(nodePairs, numNodes, nodeMask | (1<<n2), node + 1):
            yield result
        nodePairs.pop()

def pm_kind_iterator(numNodes):
    for result in _pm_iterator([], numNodes, 0, 0):
        yield result


# check pm_kind_iterator

for i,result in enumerate(pm_kind_iterator(6)):
    print(f"{i}: {result}")

for n in range(4,14,2):
    print(f"kinds for {n} =",sum(1 for x in pm_kind_iterator(n)))


def add_pm_with_color(edgelist, nodePairs, color):
    for n1,n2 in nodePairs:
        edgelist.append(Edge((n1,color),(n2,color)))

"""
# don't assume in a minimum arrangement, two pm form a circle 
for numNodes in range(6, 14, 2):
    pm_kinds = [pm for pm in pm_kind_iterator(numNodes)]
    min_pm_count = 10**10
    min_edgelist = []

    edgelist0 = []
    add_pm_with_color(edgelist0, pm_kinds[0], 0)
    for pm1 in pm_kinds:
        edgelist1 = list(edgelist0)
        add_pm_with_color(edgelist1, pm1, 1)
        for pm2 in pm_kinds:
            edgelist2 = list(edgelist1)
            add_pm_with_color(edgelist2, pm2, 2)

            pminfo = calculate_perfect_matching_info(edgelist2)
            num_pm = sum(len(pmlist) for clist,pmlist in pminfo.components.items())
            if num_pm < min_pm_count:
                min_pm_count = num_pm
                min_edgelist = edgelist2
    print("numNodes=",numNodes,"  min_pm_count=",min_pm_count)
    draw_graph(min_edgelist)
    plt.show()
"""

# assume in a minimum arrangement, two pm form a circle 
for numNodes in range(6, 20, 2):
    pm_kinds = [pm for pm in pm_kind_iterator(numNodes)]
    min_pm_count = 10**10
    min_edgelist = []

    edgelist1 = []
    # add two perfect matches that form one cycle
    for n in range(0,numNodes,2):
        edgelist1.append(Edge((n,0),(n+1,0)))
        edgelist1.append(Edge((n+1,1),((n+2)%numNodes,1)))

    for pm2 in pm_kinds:
        edgelist2 = list(edgelist1)
        add_pm_with_color(edgelist2, pm2, 2)

        pminfo = calculate_perfect_matching_info(edgelist2)
        num_pm = sum(len(pmlist) for clist,pmlist in pminfo.components.items())
        if num_pm < min_pm_count:
            min_pm_count = num_pm
            min_edgelist = edgelist2
    print("numNodes=",numNodes,"  min_pm_count=",min_pm_count)
    draw_graph(min_edgelist)
    plt.show()
        
            

"""
    # add two perfect matches with no 
    for n in range(0,numNodes,2):
        edgelist.append(Edge((n,0),(n+1,0)))
        edgelist.append(Edge((n+1,1),((n+2)%numNodes,1)))
"""



'''
    # extract perfect matches,
    # this is essentially a depth first traversal
    def _recurse_collect_pm(collection, currentTerm, currentNode):
        while (currentTerm.nodemask & (1 << currentNode)):
            currentNode += 1
        if currentNode < numNodes:
            for term in minNodeTerms[currentNode]:
                term2 = currentTerm.try_combine_term(term)
                if term2:
                    _recurse_collect_pm(collection, term2, currentNode+1)
        else:
            # got a perfect matching
            collection[currentTerm.clist(nodeIndex)].append(currentTerm)

    components = defaultdict(list)  # tuple(clist) -> list(pm)
    _recurse_collect_pm(components, MaskedMatchingTerm(numNodes), 0)
'''

