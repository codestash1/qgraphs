
from qgraphs import *
from qgraphs.draw import *


edgelist = [
    Edge("0b","1b"), Edge("0r","1r"),
    Edge("2b","5b"), Edge("2r","5r"),
    Edge("3b","4b"), Edge("3r","4r"),
    Edge("0b","4r"), Edge("4b","5r"), Edge("5b","0r"),
    Edge("1b","3r",-1), Edge("3b","2r",-1), Edge("2b","1r",-1),
]

pminfo = calculate_perfect_matching_info(edgelist)
print("state summary:", pminfo.summary)

draw_graph(edgelist)

fig = plt.gcf()
fig.savefig('out.png')
plt.show()

'''
for clist, pmlist in pminfo.components.items():
    print("clist:",clist)
    for pm in pmlist:
        print("  weight:",pm.weight())
        draw_graph(pm.edges)
        plt.show()
'''

draw_graph_components(pminfo)
fig = plt.gcf()
fig.savefig('all.png')
plt.show()

'''
for clist in pminfo.components:
    draw_graph_components(pminfo, only_clist=clist)
    plt.show()
'''

