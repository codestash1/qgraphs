
'''
?? scaling is screwed up for draw_graph_components

this is a smaller test case to try to understand

looks like maybe savefig before plt.show and after are different?
'''



import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon


def draw_something(ax):
    points = [(0.2,0.8), (0.8,0.8), (0.8,0.2), (0.5,0.5)]
    ax.axis("off")
    ax.set_aspect('equal')
    ax.add_patch(Polygon(np.array(points), closed=None, fill=None, linewidth=8,
                         edgecolor='b', zorder=1))
    opts = dict(size=12, ha='center', va='center', 
                bbox=dict(boxstyle="circle,pad=0.3", lw=1, edgecolor='k', facecolor='r'),
                zorder=2)
    for i,point in enumerate(points):
        ax.text(point[0], point[1], str(i), **opts)


fig = plt.figure(figsize=(4.0, 4.0))
ax = fig.add_subplot(1,1,1)
draw_something(ax)

fig.savefig('1.png')
plt.show()
fig.savefig('1b.png')

xtiles,ytiles = 2,4
fig = plt.figure(figsize=(4.0*xtiles, 4.0*ytiles))
axes = fig.subplots(nrows=ytiles, ncols=xtiles)
for ax in axes.flatten():
    draw_something(ax)

fig.savefig('2.png')
plt.show()
fig.savefig('2b.png')

