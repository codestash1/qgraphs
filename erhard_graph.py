
from qgraphs import *
from qgraphs.draw import *

# This graph is interesting in that it is a possible counter-example
# to the product rule constructability rule.


# from Fig3 in https://mariokrenn.files.wordpress.com/2019/08/graphquestions.pdf
# nodes relabelled to index from 0
# rescaled weights to use integers
#   ... well, guess and checked, to get new values.
#       further below records some work to actually rescale step by step
# gives state:  2 ({000000} + {111111} + {222222}) x {0000}
edgelist = [
    Edge("0r","1r"), Edge("0g","2g"), Edge("0b","9r",-1),
    Edge("1b","2b"), Edge("1g","6r"), Edge("1g","7r"),
    Edge("2r","6r",-1), Edge("2r","7r"), Edge("2r","9r"),
    Edge("3b","4b"), Edge("3g","5g"), Edge("3r","9r"),
    Edge("4r","5r"), Edge("4g","6r"), Edge("4g","7r"),
    Edge("5b","6r"), Edge("5b","7r",-1),
    Edge("6r","8r"),
    Edge("7r","8r",-1),
    Edge("8r","9r"),
]


def cnode_scale(edgelist, cnode, scale):
    out = []
    for edge in edgelist:
        if cnode in edge.cnodes:
            w = scale * edge.weight
            if isinstance(w,complex) and w.imag == 0:
                w = w.real
            if isinstance(w,float) and int(w) == w:
                w = int(w)
            out.append(Edge(edge.cnodes[0], edge.cnodes[1], w))
        else:
            out.append(edge)
    return out

if 1:
    """
    original graph weights
    although nodes still relabelled to index from 0
    """
    edgelist = [
        Edge("0r","1r",1j/2), Edge("0g","2g",1j/2), Edge("0b","9r",-1/2),
        Edge("1b","2b"), Edge("1g","6r"), Edge("1g","7r",1j),
        Edge("2r","6r",-1j), Edge("2r","7r",-1), Edge("2r","9r",1j),
        Edge("3b","4b"), Edge("3g","5g"), Edge("3r","9r"),
        Edge("4r","5r"), Edge("4g","6r",1j), Edge("4g","7r",-1),
        Edge("5b","6r",-1), Edge("5b","7r",1j),
        Edge("6r","8r",1j),
        Edge("7r","8r"),
        Edge("8r","9r",1j),
    ]

    if 1:
        """
        red monochrome edges
          complex weight
            Edge("0r","1r",1j/2),
            Edge("2r","6r",-1j),
            Edge("6r","8r",1j),
            Edge("8r","9r",1j),
            Edge("2r","9r",1j),

          real valued weight
            Edge("2r","7r",-1),
            Edge("3r","9r"),
            Edge("4r","5r"),
            Edge("7r","8r"),

        other monochrome weights
            Edge("0g","2g",1j/2),

            Edge("1b","2b"),
            Edge("3b","4b"),

            Edge("3g","5g"),

        we can scale some cnodes by j to switch between complex,real
        so we need to figure out which cnodes to scale:
            to change red monochrome complex --> real,
            while keeping red monochrome real -> real
        this is basically a parity problem
        need parity of changes on real values to be 0, and on complex to be 1

        looking at complex
           if 2 changed -> not 6,9, so must be 8
              (0 or 1) and 2,8
           else 2 not changed -> 6,9, so not 8
              (0 or 1) and 6,9
        looking at real
           (0,1) does not matter
           (4,5) can both be flipped without affecting complex or real
           (2,8) --> (7)  # okay with complex
           (6,9) --> (3)  # okay with complex
        overall, options are
           (0 or 1) and ((2,7,8) or (3,6,9)) and ((4,5) or ())
        """
        # choose (0,2,7,8) for scaling,
        # and scale in -j,j pairs so that components stay same sign
        edgelist = cnode_scale(edgelist, CNode("0r"),-1j)
        edgelist = cnode_scale(edgelist, CNode("2r"),1j)
        edgelist = cnode_scale(edgelist, CNode("7r"),-1j)
        edgelist = cnode_scale(edgelist, CNode("8r"),1j)

        """
        remaining complex edges
        Edge(cnodes=(CNode(node='0', color='g'), CNode(node='2', color='g')), weight=0.5j)
        Edge(cnodes=(CNode(node='4', color='g'), CNode(node='6', color='r')), weight=1j)
        Edge(cnodes=(CNode(node='4', color='g'), CNode(node='7', color='r')), weight=(-0-1j))

        so scale 4g and (0g or 2g)

        other edges using those cnodes: None
        """
        edgelist = cnode_scale(edgelist, CNode("4g"),-1j)
        edgelist = cnode_scale(edgelist, CNode("0g"),1j)

        """
        here are the edges used in the non-cancelling pms:
        {rrrrrrrrrr}
          Edge(cnodes=(CNode(node='0', color='r'), CNode(node='1', color='r')), weight=0.5)
          Edge(cnodes=(CNode(node='2', color='r'), CNode(node='6', color='r')), weight=1)
          Edge(cnodes=(CNode(node='3', color='r'), CNode(node='9', color='r')), weight=1)
          Edge(cnodes=(CNode(node='4', color='r'), CNode(node='5', color='r')), weight=1)
          Edge(cnodes=(CNode(node='7', color='r'), CNode(node='8', color='r')), weight=1)

          Edge(cnodes=(CNode(node='2', color='r'), CNode(node='7', color='r')), weight=-1)
          Edge(cnodes=(CNode(node='6', color='r'), CNode(node='8', color='r')), weight=-1)

        {ggggggrrrr}
          Edge(cnodes=(CNode(node='0', color='g'), CNode(node='2', color='g')), weight=-0.5)
          Edge(cnodes=(CNode(node='1', color='g'), CNode(node='6', color='r')), weight=1)
          Edge(cnodes=(CNode(node='3', color='g'), CNode(node='5', color='g')), weight=1)
          Edge(cnodes=(CNode(node='4', color='g'), CNode(node='7', color='r')), weight=1)
          Edge(cnodes=(CNode(node='8', color='r'), CNode(node='9', color='r')), weight=-1)

          Edge(cnodes=(CNode(node='4', color='g'), CNode(node='6', color='r')), weight=1)
          Edge(cnodes=(CNode(node='1', color='g'), CNode(node='7', color='r')), weight=1)

        {bbbbbbrrrr}
          Edge(cnodes=(CNode(node='0', color='b'), CNode(node='9', color='r')), weight=-0.5)
          Edge(cnodes=(CNode(node='1', color='b'), CNode(node='2', color='b')), weight=1)
          Edge(cnodes=(CNode(node='3', color='b'), CNode(node='4', color='b')), weight=1)
          Edge(cnodes=(CNode(node='5', color='b'), CNode(node='7', color='r')), weight=1)
 reuse >  Edge(cnodes=(CNode(node='6', color='r'), CNode(node='8', color='r')), weight=-1)

          Edge(cnodes=(CNode(node='5', color='b'), CNode(node='6', color='r')), weight=-1)
 reuse >  Edge(cnodes=(CNode(node='7', color='r'), CNode(node='8', color='r')), weight=1)

        remaining edges:
          Edge(cnodes=(CNode(node='2', color='r'), CNode(node='9', color='r')), weight=-1)
        """

        for edge in edgelist:
            print(edge)


pminfo = calculate_perfect_matching_info(edgelist)
print("state summary:", pminfo.summary)
print("perfect matches:")
for clist, pmlist in pminfo.components.items():
    for term in pmlist:
        print(f"  {clist} w:{term.weight()}")
print(" ----- ")

draw_graph(edgelist)

fig = plt.gcf()
fig.savefig('out.png')
plt.show()


# print out details for use in groebner calculation

def edge_name(edge):
    n1,c1 = edge.cnodes[0]
    n2,c2 = edge.cnodes[1]
    return "w{}{}{}{}".format(str(n1),str(n2),str(c1),str(c2))

def clist_name(clist):
    return "p" + "".join(str(c) for c in clist)


print("edges:")
print(", ".join(edge_name(edge) for edge in edgelist))

for clist,pmlist in pminfo.components.items():
    tlist = []
    for term in pmlist:
        tlist.append("*".join(edge_name(edge) for edge in term.edges))
    print("poly " + clist_name(clist) + "=\n    " + "\n  + ".join(tlist) + ";")

