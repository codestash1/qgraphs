
# QGraphs

Code for playing with "qgraphs" representing quantum states in optical experiments.


## Setup

```
pip3 install numpy matplotlib

# needed this to get graphics on wsl1
sudo apt-get install python3-tk
```

## Test

work in progress...

This will show a graph
```
python3 test.py
```

