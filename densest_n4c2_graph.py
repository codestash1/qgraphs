
from qgraphs import *
from qgraphs.draw import *

# This graph is interesting in that it has the most non-zero edges
# possible for a monochromatic n=4 c=2 graph.
# So it is "maximally dense"

edgelist = [
    # use red = color 0, blue = color 1
    Edge("1b","3r",1j/2),   #I2[30]=4*w1310^2+1

    Edge("2b","3b",2),      #I2[1]=w2311-2
    Edge("2r","3r",-1),     #I2[4]=w2300+1
    Edge("1b","3b",-1/2),   #I2[5]=2*w1311+1
    Edge("1r","3b",-1j),    #I2[6]=w1301+2*w1310
    Edge("1r","3r",1),      #I2[7]=w1300-1
    Edge("1b","2b",-1/2),   #I2[8]=2*w1211+1
    Edge("1b","2r",-1j/2),  #I2[9]=w1210+w1310
    Edge("1r","2b",1j),     #I2[10]=w1201-2*w1310
    Edge("1r","2r",1),      #I2[11]=w1200-1
    Edge("0b","3b",1),      #I2[12]=w0311-1
    Edge("0b","3r",1j),     #I2[13]=w0310-2*w1310
    Edge("0r","3b",1j),     #I2[14]=w0301-2*w1310
    Edge("0r","3r",1),      #I2[15]=w0300-1
    Edge("0b","2b",1),      #I2[16]=w0211-1
    Edge("0b","2r",-1j),    #I2[17]=w0210+2*w1310
    Edge("0r","2b",-1j),    #I2[18]=w0201+2*w1310
    Edge("0r","2r",1),      #I2[19]=w0200-1
    Edge("0b","1b",1),      #I2[20]=w0111-1
    Edge("0r","1r",1),      #I2[21]=w0110
]


def cnode_scale(edgelist, cnode, scale):
    out = []
    for edge in edgelist:
        if cnode in edge.cnodes:
            w = scale * edge.weight
            if isinstance(w,complex) and w.imag == 0:
                w = w.real
            if isinstance(w,float) and int(w) == w:
                w = int(w)
            out.append(Edge(edge.cnodes[0], edge.cnodes[1], w))
        else:
            out.append(edge)
    return out

if 0:
    # original result from groebner calculation and some arbitrary choices of 0,1
    edgelist = [
        # use red = color 0, blue = color 1
        Edge("1b","3r",1j/2),   #I2[30]=4*w1310^2+1

        Edge("2b","3b",2),      #I2[1]=w2311-2
        Edge("2r","3r",-1),     #I2[4]=w2300+1
        Edge("1b","3b",-1/2),   #I2[5]=2*w1311+1
        Edge("1r","3b",-1j),    #I2[6]=w1301+2*w1310
        Edge("1r","3r",1),      #I2[7]=w1300-1
        Edge("1b","2b",-1/2),   #I2[8]=2*w1211+1
        Edge("1b","2r",-1j/2),  #I2[9]=w1210+w1310
        Edge("1r","2b",1j),     #I2[10]=w1201-2*w1310
        Edge("1r","2r",1),      #I2[11]=w1200-1
        Edge("0b","3b",1),      #I2[12]=w0311-1
        Edge("0b","3r",1j),     #I2[13]=w0310-2*w1310
        Edge("0r","3b",1j),     #I2[14]=w0301-2*w1310
        Edge("0r","3r",1),      #I2[15]=w0300-1
        Edge("0b","2b",1),      #I2[16]=w0211-1
        Edge("0b","2r",-1j),    #I2[17]=w0210+2*w1310
        Edge("0r","2b",-1j),    #I2[18]=w0201+2*w1310
        Edge("0r","2r",1),      #I2[19]=w0200-1
        Edge("0b","1b",1),      #I2[20]=w0111-1
        Edge("0r","1r",1),      #I2[21]=w0110
    ]

    if 1:
        """
        complex valued edges
        Edge("1b","3r",1j/2),
        Edge("1r","3b",-1j),
        Edge("1b","2r",-1j/2),
        Edge("1r","2b",1j),
        Edge("0b","3r",1j),
        Edge("0r","3b",1j),
        Edge("0b","2r",-1j),
        Edge("0r","2b",-1j),
        """


        """
        Edge("1b","3r",1j/2),

        Edge("2b","3b",2),
        Edge("2r","3r",-1),
        Edge("1b","3b",-1/2),
        Edge("1r","3b",-1j),
        Edge("1r","3r"),
        Edge("1b","2b",-1/2),
        Edge("1b","2r",-1j/2),
        Edge("1r","2b",1j),
        Edge("1r","2r",1),
        Edge("0b","3b",1),
        Edge("0b","3r",1j),
        Edge("0r","3b",1j),
        Edge("0r","3r",1),
        Edge("0b","2b",1),
        Edge("0b","2r",-1j),
        Edge("0r","2b",-1j),
        """

        """
        ... example scaling usage
        # choose (0,2,7,8) for scaling,
        # and scale in -j,j pairs so that components stay same sign
        edgelist = cnode_scale(edgelist, CNode("0r"),-1j)
        edgelist = cnode_scale(edgelist, CNode("2r"),1j)
        edgelist = cnode_scale(edgelist, CNode("7r"),-1j)
        edgelist = cnode_scale(edgelist, CNode("8r"),1j)

        """

        for edge in edgelist:
            print(edge)


pminfo = calculate_perfect_matching_info(edgelist)
print("state summary:", pminfo.summary)
print("perfect matches:")
for clist, pmlist in pminfo.components.items():
    for term in pmlist:
        print(f"  {clist} w:{term.weight()}")
print(" ----- ")

draw_graph(edgelist)

fig = plt.gcf()
fig.savefig('out.png')
plt.show()

