
"""
assumes ./Singular is symlink to Singular binary
also requires enumerate_monocolor.py has already precalculated edgelists
"""

import os
import sys
import itertools
import pickle
import subprocess
import time
sys.path.append("../..")
from qgraphs import *
from qgraphs.draw import *


numNodes, numColors, maxNumColors = 4,2,2
node0scale = True
requireC3compatible = True

# unbuffer output
import io
sys.stdout = io.TextIOWrapper(open(sys.stdout.fileno(), 'wb', 0), write_through=True)


save_data_fname = f"data_n{numNodes}c{numColors}.pickle"
with open(save_data_fname,"rb") as f:
    collect = pickle.load(f)


def edge_name(edge):
    n1,c1 = edge.cnodes[0]
    n2,c2 = edge.cnodes[1]
    return "w{}{}{}{}".format(str(n1),str(n2),str(c1),str(c2))

def inv_edge_name(edge):
    return edge_name(edge).replace("w","v")

def clist_name(clist):
    return "p" + "".join(str(c) for c in clist)



clists = []
for c in range(maxNumColors):
    clists.append([c]*numNodes)
clists = tuple(tuple(clist) for clist in clists)

edgelist = []
for n1 in range(numNodes):
    for n2 in range(n1+1,numNodes):
        for c1 in range(maxNumColors):
            for c2 in range(maxNumColors):
                edge = Edge((n1,c1),(n2,c2))
                edgelist.append(edge)

pminfo = calculate_perfect_matching_info(edgelist)

vlist = [edge_name(edge) for edge in edgelist]
for edge in edgelist:
    if edge.cnodes[0].color == edge.cnodes[1].color:
        vlist.append(edge_name(edge).replace('w','v'))

f = open("temp_setup.s","w")
f.write("// n={} c={} maxc={}\n".format(numNodes, numColors, maxNumColors))
f.write("// num variables: {}\n".format(len(vlist)))
group = maxNumColors*maxNumColors
pieces = [", ".join(vlist[x:x+group]) for x in range(0,len(vlist),group)]
f.write("ring R = QQ[\n    " + ",\n    ".join(pieces) + "];\n")

constraint_clists = [clist for clist in pminfo.components]
constraint_clists.sort()
plist = []
plistExtra = []
for clist in constraint_clists:
    largeColors = False
    if numColors < maxNumColors:
        if any(c >= numColors for c in clist):
            largeColors = True
    pname = clist_name(clist)
    if clist in clists:
        pconstraint = "{} - 1".format(pname)
    else:
        pconstraint = "{}".format(pname)
    if not largeColors:
        plist.append(pconstraint)
    else:
        plistExtra.append(pconstraint)
    pmlist = pminfo.components[clist]
    tlist = []
    for term in pmlist:
        tlist.append("*".join(edge_name(edge) for edge in term.edges))
    f.write("poly " + pname + "=\n    " + "\n  + ".join(tlist) + ";\n")

f.write("ideal I0=\n  " + ",\n  ".join(plist) + ";\n")
if plistExtra:
    f.write("ideal I1=\n  " + ",\n  ".join(plistExtra) + ";\n")

# ideal of monomials we can derive from I0
helperIdeals = []

f.write("ideal proof2n1 =\n")
helperIdeals.append("proof2n1")
tlist = []
for n1 in range(numNodes):
    othernodes = tuple(n for n in range(numNodes) if n!=n1)
    for c1 in range(maxNumColors):
        othercolors = tuple(c for c in range(maxNumColors) if c!=c1)
        choices = [othercolors]*numNodes
        choices[n1] = (c1,)
        for clist in itertools.product(*choices):
            vlist = []
            for n2 in othernodes:
                vlist.append(edge_name(Edge((n1, c1), (n2, clist[n2]))))
            tlist.append("*".join(vlist))
f.write("  " + ",\n  ".join(tlist) + ";\n")

if maxNumColors >=3 or requireC3compatible:
    if maxNumColors >= 3:
        f.write("ideal proof2n =\n")
        helperIdeals.append("proof2n")
        tlist = []
        for n1 in range(numNodes):
            othernodes = tuple(n for n in range(numNodes) if n!=n1)
            for c1 in range(maxNumColors):
                othercolors = tuple(c for c in range(maxNumColors) if c!=c1)
                choices = [othercolors]*numNodes
                for clist in itertools.product(*choices):
                    if all(c==clist[0] for c in clist):
                        continue
                    vlist = []
                    for n2 in othernodes:
                        vlist.append(edge_name(Edge((n1, clist[n1]), (n2, clist[n2]))))
                    tlist.append("*".join(vlist))
        f.write("  " + ",\n  ".join(tlist) + ";\n")

    f.write("ideal proofDegn2 =\n")
    helperIdeals.append("proofDegn2")
    tlist = []
    for n1 in range(numNodes):
        othernodes = tuple(n for n in range(numNodes) if n!=n1)
        for c1,c2 in itertools.combinations(range(maxNumColors),2):
            for remove in othernodes:
                nodechoices = tuple(n for n in othernodes if n!=remove)
                vlist = []
                for n2 in nodechoices:
                    vlist.append(edge_name(Edge((n1, c1), (n2, c2))))
                tlist.append("*".join(vlist))
    f.write("  " + ",\n  ".join(tlist) + ";\n")

    f.write("ideal proofMono =\n")
    helperIdeals.append("proofMono")
    tlist = []
    for c in range(maxNumColors):
        for n1 in range(numNodes):
            othernodes = tuple(n for n in range(numNodes) if n!=n1)
            vlist = []
            for n2 in othernodes:
                vlist.append(edge_name(Edge((n1, c), (n2, c))))
            tlist.append("*".join(vlist))
    f.write("  " + ",\n  ".join(tlist) + ";\n")

f.write("ideal helpI = " + ", ".join(helperIdeals) + ";\n")
f.write("option(redSB);\n")
f.close()


temp_run_fname = "temp_run.s"
skip = False
for i,edgelist in enumerate(collect):
    outfname = f"{i}.out"
    if os.path.exists(outfname):
        skip = True
        continue
    if skip:
        print(f"... skip to #{i}")
        skip=False
    start = time.time()
    print(edgelist)
    
    if 0:
        draw_graph([Edge(*edge) for edge in edgelist])
        plt.show()

    eset = {Edge(*edge) for edge in edgelist}
    with open(temp_run_fname,"w") as f:
        f.write('// load constants\n< "temp_setup.s";\n')
        f.write("//option(prot);\n") # would be nice if this went to stderr by default
        extra_constraints = []
        for n1 in range(numNodes):
            for n2 in range(n1+1,numNodes):
                for c in range(numColors):
                    edge = Edge((n1,c),(n2,c))
                    if edge in eset:
                        if node0scale and n1 == 0:
                            extra_constraints.append("{} - 1".format(
                                edge_name(edge)))
                        else:
                            extra_constraints.append("{}*{} - 1".format(
                                edge_name(edge), inv_edge_name(edge)))
                    else:
                        extra_constraints.append("{}".format(
                            edge_name(edge)))
        f.write("ideal I = I0, helpI,\n")
        f.write("  " + ",\n  ".join(extra_constraints) + ";\n")
        f.write("ideal I2 = slimgb(I);\n")
        f.write("I2;\n")
        if numColors < maxNumColors:
            f.write("ideal I3 = I1, I2;\n")
            f.write("ideal I4 = slimgb(I3);\n")
            f.write("I4;\n")

    out = subprocess.run(["./Singular","-q"], check=True,
                        stdin=open(temp_run_fname,"rb"), stdout=subprocess.PIPE, stderr=None)
    #print("  out =",out)
    runtime = time.time() - start
    has_solution = not (b"I2[1]=1\n" in out.stdout)
    if 1 and (b"I4[1]=w" in out.stdout):
        draw_graph([Edge(*edge) for edge in edgelist])
        plt.show()

    print(f"  result #{i} : {has_solution} : time={runtime}")
    with open(outfname, "wb") as f:
        f.write(out.stdout)


