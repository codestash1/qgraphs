
'''
Goal:
enumerate all "symmetry unique" graphs which only use monochrome edges

if two graphs are related by a mere node relabelling and/or color relabelling,
that is not interesting.

This is done by tracking "cnode degree" data for each node,
    which is information about the edges touching this node: (total, color 0, color 1, ... )

For any graph that exists, there is a symmetry related one in which this data is "sorted"
according to node and color number.

So to efficiently iterate through the symmetry unique graphs,
 - we only need to consider those which are sorted
 - and we can build the graphs by recursively adding edges such that we hit each possible
    sorted "cnode degree" only once

'''
import sys
sys.path.append('../..')
from qgraphs import *
import pickle


def _check1_cnodeDegrees_index(cnodeDegrees, idx):
    # eliminate node relabelling symmetry by requiring:
    # cnodeDegree entries should be decreasing
    if idx>0 and cnodeDegrees[idx] > cnodeDegrees[idx-1]:
        #print(f"reject1({idx}) : {cnodeDegrees}")
        return False
    return True

def _check2_cnodeDegrees_index(cnodeDegrees, idx):
    if any(cnodeDegrees[i+1] > cnodeDegrees[i] for i in range(idx)):
        #print(f"reject2a({idx}) : {cnodeDegrees}")
        return False

    # eliminate color relabelling symmetry by requiring:
    # colors should be distinquished by decreasing
    # this is complicated, so for now just always start from the beginning

    # check : list of ranges which are required to be in decreasing order
    check = [(1,len(cnodeDegrees[0]))]
    for j,cnodeDeg in enumerate(cnodeDegrees):
        if j > idx:
            break
        newcheck = []
        for vstart, vend in check:
            val = cnodeDeg[vstart]
            newvstart = vstart
            for i in range(vstart+1,vend):
                val2 = cnodeDeg[i]
                #print("cnDeg:{} check:{} i:{} val:{} val2:{}".format(cnodeDeg,check,i,val,val2))
                if val2 > val:
                    #print(f"reject2b({idx},{i}) : {cnodeDegrees}")
                    return False
                if val2 < val:
                    if i-newvstart > 1:
                        newcheck.append((newvstart,i))
                    val = val2
                    newvstart = i
            if vend - newvstart > 1:
                newcheck.append((newvstart,vend))
        if len(newcheck) == 0:
            break
        check = newcheck
    return True


def _recurse(edges, cnodeDegrees, node, node2start, color, numNodes, numColors):
    #print("Enter: cnDegs={} node={} node2start={} color={}".format(
    #      cnodeDegrees, node, node2start, color))

    if node >= numNodes:
        # done, cannot add any more
        return

    # first try moving onto the next cnode,
    # so that we generate the lower degree results first
    if color < numColors - 1:
        nextcolor = color + 1
        nextnode = node
    else:
        nextcolor = 0
        nextnode = node + 1
        if not _check2_cnodeDegrees_index(cnodeDegrees, node):
            return
    for result in _recurse(edges, cnodeDegrees,
                           nextnode, nextnode+1, nextcolor, numNodes, numColors):
        yield result

    # don't copy degree info here, we'll just readjust afterwards
    cnDeg = cnodeDegrees[node]
    cnDeg[0] += 1 # first entry is total degree
    cnDeg[color+1] += 1 # next entries are degree for each color

    if _check1_cnodeDegrees_index(cnodeDegrees, node):
        for node2 in range(node2start, numNodes):
            # don't make copies, we'll just readjust afterwards
            cnDeg2 = cnodeDegrees[node2]
            cnDeg2[0] += 1 # first entry is total degree
            cnDeg2[color+1] += 1 # next entries are degree for each color

            if 1: #_check1_cnodeDegrees_index(cnodeDegrees, node2):
                # don't make copies, we undo edgelist changes afterwards
                newedge = ((node,color),(node2,color))
                edges.append(newedge)

                if _check2_cnodeDegrees_index(cnodeDegrees, numNodes-1):
                    yield (tuple(edges), tuple(tuple(x) for x in cnodeDegrees))
                for result in _recurse(edges, cnodeDegrees,
                                       node, node2+1, color, numNodes, numColors):
                    yield result

                # undo edgelist changes
                edges.pop()

            # undo degree changes
            cnDeg2[0] -= 1
            cnDeg2[color+1] -= 1
    
    # undo degree changes
    cnDeg[0] -= 1
    cnDeg[color+1] -= 1


def iterate_monocolor_edge_graphs(numNodes, numColors):
    cnodeDegs = [[0]*(numColors+1) for _ in range(numNodes)]
    yield (tuple(), tuple(tuple(x) for x in cnodeDegs))
    for result in _recurse([], cnodeDegs, 0, 1, 0, numNodes, numColors):
        yield result

if 0:
    # example sorting check, result should be false
    test=((2, 1, 0, 1), (2, 1, 0, 1), (0, 0, 0, 0), (0, 0, 0, 0))
    print("test:",test,"   -->", _check_cnodeDegrees_index(test,2))

#collect = set()  # switch to this for "filter duplicates"
collect = []
count = 0
numNodes, numColors = 4,2
for edges, cnodeDegs in iterate_monocolor_edge_graphs(numNodes, numColors):
    # first filter
    if 1:
        if any(cnDeg.count(0) for cnDeg in cnodeDegs):
            continue

    # second filter, require at least one perfect matching for each color
    if 1:
        edges_by_color = {c:[] for c in range(numColors)}
        for edge in edges:
            edges_by_color[edge[0][1]].append(Edge(*edge))
        found_bad = False
        for c in range(numColors):
            pminfo = calculate_perfect_matching_info(edges_by_color[c])
            if len(pminfo.components) == 0:
                found_bad = True
                break
        if found_bad:
            continue
        #print(edges)

    # third filter, remove any possible duplicates
    if isinstance(collect, set):
        if edges in collect:
            continue
        edges = tuple(sorted(edges))
        collect.add(edges)
    else:
        collect.append(edges)

    count += 1
    #print(cnodeDegs)
    if count % 100000 == 0:
        print(count)
print("total:", count)

if isinstance(collect, set):
    collect = sorted(collect)
save_data_fname = f"data_n{numNodes}c{numColors}.pickle"
with open(save_data_fname,"wb") as f:
    pickle.dump(collect, f)

print("save complete")

