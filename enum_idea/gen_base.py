
"""
generates base files for Singular scripts
which hold common definitions useful for monochromatic graphs
"""

import itertools


# simplified Edge definition,
# so we do not need to pull in library just for this
class Edge:
    def __init__(self, cnode1, cnode2):
        if cnode1 > cnode2:
            cnode1, cnode2 = cnode2, cnode1
        self.cnodes = (cnode1, cnode2)


def edge_name(edge):
    n1,c1 = edge.cnodes[0]
    n2,c2 = edge.cnodes[1]
    return "w{}{}{}{}".format(str(n1),str(n2),str(c1),str(c2))

def inv_edge_name(edge):
    return edge_name(edge).replace("w","v")

def clist_name(clist):
    return "p" + "".join(str(c) for c in clist)



def perfectMatchings(n):
    """
    calculate all perfect matchings on K_n
    returns list of matchings: list of edges: tuple of node numbers
    """
    if n%2 != 0:
        raise ValueError("number of nodes should be even")
    matchings = _perfectMatchings(range(n))
    matchings.sort()
    return matchings

def _perfectMatchings(nodelist):
    """recursively find perfect matchings on remaining nodes"""
    # base case, only two nodes left
    if len(nodelist) == 2:
        return [[tuple(nodelist),],]

    matchings = []
    # loop over ways to match first node with other nodes
    for n in nodelist[1:]:
        mStart = [(nodelist[0],n),]
        # get list of what nodes are left to match
        nextlist = list(nodelist[1:])
        nextlist.remove(n)
        # recursively find all the matchings of remaining nodes
        # and combine with our starting match choice
        for mSubgraph in _perfectMatchings(nextlist):
            mCombined = list(mStart)
            mCombined.extend(mSubgraph)
            matchings.append(mCombined)
    return matchings



def gen_base(numNodes):
    numColors = 2
    maxNumColors = 3
    pmlist = perfectMatchings(numNodes)

    clists = []
    for c in range(maxNumColors):
        clists.append([c]*numNodes)
    clists = tuple(tuple(clist) for clist in clists)

    edgelist = []
    for n1 in range(numNodes):
        for n2 in range(n1+1,numNodes):
            for c1 in range(maxNumColors):
                for c2 in range(maxNumColors):
                    edge = Edge((n1,c1),(n2,c2))
                    edgelist.append(edge)

    vlist = []
    for edge in edgelist:
        vlist.append(edge_name(edge))
    for edge in edgelist:
        vlist.append(inv_edge_name(edge))

    with open(f"base_n{numNodes}.s", "w") as f:
        f.write("// n={} c={} maxc={}\n".format(numNodes, numColors, maxNumColors))
        f.write("// num edge weights = {}\n".format(len(vlist)//2))
        f.write("// num vertex colorings = {}\n".format(maxNumColors**numNodes))
        f.write("int save_echo=echo; echo=0;\n");
        f.write("option(redSB);\n")

        group = maxNumColors * maxNumColors
        pieces = [", ".join(vlist[x:x+group]) for x in range(0,len(vlist),group)]
        f.write("ring R = QQ[\n    " + ",\n    ".join(pieces) + "];\n")

        plist = []
        plistExtra = []
        for clist in itertools.product(range(maxNumColors), repeat=numNodes):
            largeColors = False
            if any(c >= numColors for c in clist):
                largeColors = True

            pname = clist_name(clist)
            if clist in clists:
                pconstraint = "{} - 1".format(pname)
            else:
                pconstraint = "{}".format(pname)

            if not largeColors:
                plist.append(pconstraint)
            else:
                plistExtra.append(pconstraint)

            tlist = []
            for nodepairs in pmlist:
                tlist.append("*".join(
                    edge_name(Edge((n1,clist[n1]),(n2,clist[n2]))) for n1,n2 in nodepairs))

            f.write("poly " + pname + "=\n    " + "\n  + ".join(tlist) + ";\n")

        f.write(f"ideal Ic2=\n  " + ",\n  ".join(plist) + ";\n")
        f.write(f"ideal Ic23=\n  " + ",\n  ".join(plistExtra) + ";\n")

        # ideal of monomials we can derive from Ic2
        helperIdealsC2 = []
        helperIdealsC23 = []
        helperIdealsC3 = []

        # "proof2n1" only requires two colors

        tlist = []
        tlistExtra = []
        for n1 in range(numNodes):
            othernodes = tuple(n for n in range(numNodes) if n!=n1)
            for c1 in range(maxNumColors):
                othercolors = tuple(c for c in range(maxNumColors) if c!=c1)
                choices = [othercolors]*numNodes
                choices[n1] = (c1,)
                for clist in itertools.product(*choices):
                    if any(c >= numColors for c in clist):
                        out = tlistExtra
                    else:
                        out = tlist

                    vlist = []
                    for n2 in othernodes:
                        vlist.append(edge_name(Edge((n1, c1), (n2, clist[n2]))))
                    out.append("*".join(vlist))

        helperIdealsC2.append("proof2n1_c2")
        f.write("ideal proof2n1_c2 =\n")
        f.write("  " + ",\n  ".join(tlist) + ";\n")

        helperIdealsC3.append("proof2n1_c3")
        f.write("ideal proof2n1_c3 =\n")
        f.write("  " + ",\n  ".join(tlistExtra) + ";\n")


        # "proof2n", "proofDegn2", "proofMono" require 3 colors
        # but can derive results which only involve 2 colors
        # so if the eventual goal is 3 colors, these can be included

        tlist = []
        tlistExtra = []
        for n1 in range(numNodes):
            othernodes = tuple(n for n in range(numNodes) if n!=n1)
            for c1 in range(maxNumColors):
                othercolors = tuple(c for c in range(maxNumColors) if c!=c1)
                choices = [othercolors]*numNodes
                for clist in itertools.product(*choices):
                    if all(c==clist[0] for c in clist):
                        continue
                    if any(c >= numColors for c in clist):
                        out = tlistExtra
                    else:
                        out = tlist

                    vlist = []
                    for n2 in othernodes:
                        vlist.append(edge_name(Edge((n1, clist[n1]), (n2, clist[n2]))))
                    out.append("*".join(vlist))

        helperIdealsC23.append("proof2n_c23")
        f.write("ideal proof2n_c23 =\n")
        f.write("  " + ",\n  ".join(tlist) + ";\n")

        helperIdealsC3.append("proof2n_c3")
        f.write("ideal proof2n_c3 =\n")
        f.write("  " + ",\n  ".join(tlistExtra) + ";\n")


        tlist = []
        tlistExtra = []
        for n1 in range(numNodes):
            othernodes = tuple(n for n in range(numNodes) if n!=n1)
            for c1,c2 in itertools.product(range(maxNumColors),repeat=2):
                if c1 == c2:
                    continue
                if c1 >= numColors or c2 >= numColors:
                    out = tlistExtra
                else:
                    out = tlist

                for remove in othernodes:
                    nodechoices = tuple(n for n in othernodes if n!=remove)
                    vlist = []
                    for n2 in nodechoices:
                        vlist.append(edge_name(Edge((n1, c1), (n2, c2))))
                    out.append("*".join(vlist))

        helperIdealsC23.append("proofDegn2_c23")
        f.write("ideal proofDegn2_c23 =\n")
        f.write("  " + ",\n  ".join(tlist) + ";\n")

        helperIdealsC3.append("proofDegn2_c3")
        f.write("ideal proofDegn2_c3 =\n")
        f.write("  " + ",\n  ".join(tlistExtra) + ";\n")


        tlist = []
        tlistExtra = []
        for c in range(maxNumColors):
            if c >= numColors:
                out = tlistExtra
            else:
                out = tlist
            for n1 in range(numNodes):
                othernodes = tuple(n for n in range(numNodes) if n!=n1)
                vlist = []
                for n2 in othernodes:
                    vlist.append(edge_name(Edge((n1, c), (n2, c))))
                out.append("*".join(vlist))

        helperIdealsC23.append("proofMono_c23")
        f.write("ideal proofMono_c23 =\n")
        f.write("  " + ",\n  ".join(tlist) + ";\n")

        helperIdealsC3.append("proofMono_c3")
        f.write("ideal proofMono_c3 =\n")
        f.write("  " + ",\n  ".join(tlistExtra) + ";\n")


        # include the derivations in easy helper ideals
        f.write("ideal helpC2 = " + ", ".join(helperIdealsC2) + ";\n")
        f.write("ideal helpC23 = " + ", ".join(helperIdealsC23) + ";\n")
        f.write("ideal helpC3 = " + ", ".join(helperIdealsC3) + ";\n")

        f.write("echo=save_echo;\n");
        f.close()


gen_base(4)
gen_base(6)

