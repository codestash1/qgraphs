
"""
requires:
- ./Singular is symlink to Singular binary
  - and is built with prot_stderr.patch (or code below is modified to not turn on prot option)
- brute_enumerate_monocolor.py to have already precalculated data
- gen_base.py to have generated common base scripts
"""

import os
import sys
import itertools
import pickle
import subprocess
import time
import networkx as nx
from collections import defaultdict
sys.path.append("..")
from qgraphs import *
from qgraphs.draw import *

if len(sys.argv) < 2:
    print("Usage: python3 solve_c2.py numNodes")
    sys.exit(1)

numNodes = int(sys.argv[1])
numColors = 2

with open(f"brute2_n{numNodes}.pickle", "rb") as f:
    info = pickle.load(f)

# info = {"total1":len(out), "total2":total2, "out2":out2, "automorphisms1":automorphisms1}
out2 = info["out2"]
automorphisms1 = info["automorphisms1"]

list_elist1 = list(out2)
print([(i,len(out2[x])) for i,x in enumerate(list_elist1)])
print(sum(len(v) for k,v in out2.items()))
print(list_elist1[53])

for i,elist1 in enumerate(out2):
    edgelist = [Edge((n1,c1),(n2,c2)) for n1,n2,c1,c2 in elist1]
    nauto = len(automorphisms1[elist1])
    N = len(out2[elist1])
    if N>2000:
        continue

    """
    for j,elist2 in enumerate(out2[elist1]):
        edgelist = [Edge((n1,c1),(n2,c2)) for n1,n2,c1,c2 in elist1]
        edgelist.extend(Edge((n1,c1),(n2,c2)) for n1,n2,c1,c2 in elist2)
    """
    draw_graph(edgelist)
    plt.title(f"n={numNodes} c={numColors} #{i} p={nauto} N={N}")
    plt.show()


