#!/bin/bash

SCRIPT_DIR=$(dirname "$0")
#SINGULAR=$SCRIPT_DIR/Singular/Singular/Singular
SINGULAR=$SCRIPT_DIR/Singular
TIME=/usr/bin/time # do not use shell builtin

if [[ "$#" != "1" ]]; then
  echo "Usage:  run-Singular-script.sh file.s"
  echo "will run script with Singular, saving results to out_<file>.txt"
  exit 1
fi
fname="$1"
fdir=$(dirname -- "$fname")
fbase=$(basename -- "$fname")
outname="$fdir/out_${fbase%.*}.txt"
CMD="$SINGULAR -q -e --no-rc"

echo "Saving results in: $outname"
echo "+ $CMD 2>&1 < \"$fname\"" | tee "$outname"
$TIME -f "max memory(KB):%M  time(sec):%e = %E" \
    $CMD 2>&1 < "$fname" | tee -a "$outname"

