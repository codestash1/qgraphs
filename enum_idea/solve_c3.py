
"""
requires:
- ./Singular is symlink to Singular binary
  - and is built with prot_stderr.patch (or code below is modified to not turn on prot option)
- brute_enumerate_monocolor.py to have already precalculated data
- gen_base.py to have generated common base scripts
- solve_c2.py to have calculated *.out files
"""

import os
import sys
import re
import itertools
import pickle
import subprocess
import time
sys.path.append("..")
from qgraphs import *
from qgraphs.draw import *

if len(sys.argv) < 2:
    print("Usage: python3 solve_c3.py numNodes")
    sys.exit(1)

numNodes = int(sys.argv[1])


# unbuffer output
import io
sys.stdout = io.TextIOWrapper(open(sys.stdout.fileno(), 'wb', 0), write_through=True)


with open(f"brute2_n{numNodes}.pickle", "rb") as f:
    info = pickle.load(f)

# info = {"total1":len(out), "total2":total2, "out2":out2, "automorphisms1":automorphisms1}
out2 = info["out2"]
automorphisms1 = info["automorphisms1"]


varname_pattern = '([vw]....)'

def edge_name(n1,n2,c1,c2):
    if n1 < n2:
        return f"w{n1}{n2}{c1}{c2}"
    return f"w{n2}{n1}{c2}{c1}"

def inv_edge_name(n1,n2,c1,c2):
    return edge_name(n1,n2,c1,c2).replace("w","v")

def clist_name(clist):
    return "p" + "".join(str(c) for c in clist)


def load_groebner(fname):
    if not os.path.exists(fname):
        raise ValueError(f"ERROR: load_groebner : file {fname} does not exist")
    plist = []
    with open(fname, "r") as f:
        for linenum,line in enumerate(f):
            line = line.strip()
            if not line.startswith("I"):
                raise ValueError(f"ERROR: {fname} line {linenum} does not start with 'I'")
            idx = line.find("=")
            if idx < 0:
                raise ValueError(f"ERROR: {fname} line {linenum} does not contain '='")
            idx += 1
            plist.append(line[idx:])
    return plist

def permute_variable_name(s,nodePermute,colorPermute):
    if len(s) != 5 or (s[0] != "w" and s[0] != "v"):
        # not a variable name
        return s
    t,n1,n2,c1,c2 = s
    try:
        n1 = nodePermute[int(n1)]
        n2 = nodePermute[int(n2)]
        c1 = colorPermute[int(c1)]
        c2 = colorPermute[int(c2)]
    except Exception as e:
        print("ERROR:",(n1,n2,c1,c2)," --> ",str(e))
        print("nodePermute:",nodePermute)
        print("colorPermute:",colorPermute)
        t = ""
    if t=="w":
        return edge_name(n1,n2,c1,c2)
    if t=="v":
        return inv_edge_name(n1,n2,c1,c2)
    raise Exception(f"invalid variable name '{s}'")

def permute_poly_string(pstring, nodePermute, colorPermute):
    out = []
    tokens = re.split(varname_pattern, pstring)
    for tok in tokens:
        out.append(permute_variable_name(tok, nodePermute, colorPermute))
    return "".join(out)

def permute_groebner(plist, nodePermute, colorPermute):
    out = []
    for pstring in plist:
        out.append(permute_poly_string(pstring, nodePermute, colorPermute))
    return out

def permute_edges(edgelist, nodePermute, colorPermute):
    """
    edgelist : list of edge in format : tuple (n1,n1,c1,c2)
    nodePermute : list of node#    so that newNode = nodePermute[oldNode]
    colorPermute : list of node#   so that newColor = nodePermute[oldColor]
    """
    out = []
    for n1,n2,c1,c2 in edgelist:
        n1 = nodePermute[n1]
        n2 = nodePermute[n2]
        c1 = colorPermute[c1]
        c2 = colorPermute[c2]
        if n1 < n2:
            new_edge = (n1,n2,c1,c2)
        else:
            new_edge = (n2,n1,c2,c1)
        out.append(new_edge)
    out.sort()
    return tuple(out)


def run_attempt(out_fname, numNodes, plist):
    start = time.time()
    print(f"Run attempt {out_fname}")

    temp_run_fname = f"temp_run_n{numNodes}c3.s"
    with open(temp_run_fname,"w") as f:
        f.write('// load constants\n')
        f.write(f'< "base_n{numNodes}.s";\n')
        f.write("option(prot);\n")
        f.write("ideal I = Ic23, helpC3,\n")
        f.write("  " + ",\n  ".join(plist) + ";\n")
        f.write("ideal I2 = slimgb(I);\n")
        f.write("I2;\n")

    out = subprocess.run(["./Singular","-q","--cntrlc=q"], check=True,
                        stdin=open(temp_run_fname,"rb"),
                        stderr=sys.stderr,
                        stdout=subprocess.PIPE)
    #print("  out =",out)
    runtime = time.time() - start
    has_solution = not (b"I2[1]=1\n" in out.stdout)

    print(f"  result {out_fname} : {has_solution} : time={runtime}")
    with open(out_fname, "wb") as f:
        f.write(out.stdout)

    return has_solution

# =========================================================================


for i,elist1 in enumerate(out2):
    nodePermutations = automorphisms1[elist1]

    # first gather groebner details for each possibility
    jmax = len(out2[elist1])
    npossible = 0
    groebner = {}  # (i,j) -> plist
    for j in range(jmax):
        try:
            g01 = load_groebner(f"n{numNodes}c2_{i}_{j}.out")
            if len(g01) > 1:
                npossible += 1
            groebner[(i,j)] = g01
        except ValueError as e:
            print(e)
            print(f"WARNING: truncating jmax to {j} from {jmax}")
            jmax = j
            break
    print(f"c2 graphs using color0 #{i}, has jmax={jmax}, npossible={npossible}")

    # try all combinations
    for j in range(jmax):
        elist2 = out2[elist1][j]
        g01 = groebner[(i,j)]
        if len(g01) == 1:
            continue
        for k in range(j,jmax):
            g02 = groebner[(i,k)]
            if len(g02) == 1:
                continue
            elist3_org = out2[elist1][k]
            seen = set()
            for q,nodePermute in enumerate(nodePermutations):
                elist3 = permute_edges(elist3_org, nodePermute, [0,2,1])
                # eliminate some repeated work
                #   there is probably more, from color permutations
                #   ... may want to rethink approach here to remove redundant work
                if elist3 in seen:
                    continue
                seen.add(elist3)

                g = g01 + permute_groebner(g02, nodePermute, [0,2,1])
                if run_attempt(f"n{numNodes}c3_{i}_{j}_{k}p{q}.out", numNodes, g):
                    print("!!! SUCCESS !!!")
                    edgelist = [Edge((n1,c1),(n2,c2)) for n1,n2,c1,c2 in elist1]
                    edgelist.extend(Edge((n1,c1),(n2,c2)) for n1,n2,c1,c2 in elist2)
                    edgelist.extend(Edge((n1,c1),(n2,c2)) for n1,n2,c1,c2 in elist3)
                    draw_graph(edgelist)
                    plt.show()

