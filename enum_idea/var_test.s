
/*
      0--4--5
     /|\
    / | \
   1  2  3
*/
ring R = QQ[
    w01, w02, w03, w04, w45,
    s0, s1, s2, s3, s4, s5,
    gscale
    ];
ideal I =
    w01*s0*s1 - 1,
    w02*s0*s2 - 1,
    w03*s0*s3 - 1,
    w04*s0*s4 - 1,
    w45*s4*s5 - 1,
    s0*s1*s2*s3*s4*s5 - gscale;
option(redSB);
option(prot);
ideal I2 = slimgb(I);
I2;
eliminate(I2, s0*s1*s2*s3*s4*s5*gscale);
eliminate(I2, s1*s2*s3*s4*s5);
