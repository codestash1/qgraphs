
'''
Goal:
calculate all "symmetry unique" graphs which only use monochrome edges
and save results for use in later calculations

for one color:
    go over every possible combinations of edges
    then brute check all symmetry operations for "minimal" entry

for two colors:
    combine all filtered options of color 1 with permuted options of color2
    then brute check all symmetry operations for "minimal" entry


Notes:

--- n=4
number of c=1 edges = n*(n-1)/2 = 6
brute_search1 inner loop runs should be 2^(numEdges) * (n! - 1) = 64 * 23 = 1472
actual: ## inner loop runs = 1472
all brute1 = 11
after c3 filter, total1 = 3 
brute_search2 inner loop runs should be (total1*(total1+1)/2) * n! * numAutomorphisms[elist1]
    = 6 * 24 * ?? = 144 * ??
actual: ## inner loop runs = 864
    ... so "average" number of automorphisms is: 864/144 = 6
total2 = 20

--- n=6
number of c=1 edges = n*(n-1)/2 = 15
brute_search1 inner loop runs should be 2^(numEdges) * (n! - 1) = 32867 * 719 = 23560192
actual: ## inner loop runs = 23560192
all brute1 = 156
after c3 filter, total1 = 73
brute_search2 inner loop runs should be (total1*(total1+1)/2) * n! * numAutomorphisms[elist1]
    = 2701 * 720 * ?? = 1944720 * ??
actual: ## inner loop runs = 12753360
    ... so "average" number of automorphisms is: 12753360/1944720 ~ 6.56
total2 = 292652



$ time python3 brute_enumerate_monocolor.py 
...
numNodes = 4
## inner loop runs = 1472
all brute1 = 11
total1 = 3
## inner loop runs = 864
total2 = 20
wrote data to 'brute2_n4.pickle'
...
numNodes = 6
## inner loop runs = 23560192
all brute1 = 156
total1 = 73
## inner loop runs = 12753360
total2 = 292652
wrote data to 'brute2_n6.pickle'
...
real	1m24.247s
user	1m23.904s
sys	0m0.148s

'''

import sys
import itertools
from collections import defaultdict
sys.path.append('..')
from qgraphs import *
import pickle


def permute_edges(edgelist, nodePermute, colorPermute):
    """
    edgelist : list of edge in format : tuple (n1,n1,c1,c2)
    nodePermute : list of node#    so that newNode = nodePermute[oldNode]
    colorPermute : list of node#   so that newColor = nodePermute[oldColor]
    """
    out = []
    for n1,n2,c1,c2 in edgelist:
        n1 = nodePermute[n1]
        n2 = nodePermute[n2]
        c1 = colorPermute[c1]
        c2 = colorPermute[c2]
        if n1 < n2:
            new_edge = (n1,n2,c1,c2)
        else:
            new_edge = (n2,n1,c2,c1)
        out.append(new_edge)
    out.sort()
    return tuple(out)

def brute_search1(numNodes):
    """
    find all symmetry unique sets of edges in c=1
    
    go over every possible combinations of edges
    then brute check all symmetry operations for "minimal" entry
    """
    result = set()
    edges = []
    for n1 in range(numNodes):
        for n2 in range(n1+1,numNodes):
            edges.append((n1,n2,0,0))

    colorPermute = [0]
    nodePermutations = list(itertools.permutations(range(numNodes)))
    nodePermutations.remove(tuple(range(numNodes)))

    runs = 0
    for numEdges in range(len(edges)+1):
        for edgelist in itertools.combinations(edges, r=numEdges):
            current = edgelist
            for nodePermute in nodePermutations:
                runs += 1
                new = permute_edges(edgelist, nodePermute, colorPermute)
                if new < current:
                    current = new
            #print(f"org:{edgelist} --> min:{current}")
            result.add(current)
    print(f"## inner loop runs = {runs}")

    result = list(result)
    result.sort(key=len)
    return tuple(result)


def brute_search2(listc1, numNodes):
    """
    find all symmetry unique sets of edges in c=2 made from
      combinations of patterns in c=1 
    
    combine all filtered options of color 1 with permuted options of color2
    then brute check all symmetry operations for "minimal" entry

    returns: total, result, automorphisms
        total : number of combined symmetry unique edgelists
        result is a dictionary :
            tuple of color0 edges -> list of edgelists with color1
        automorphisms is a dictionary :
            tuple of color0 edges -> list of nodePermutations which preserve this edgelist

    """
    nodePermutations = list(itertools.permutations(range(numNodes)))

    # ensure the c=1 edgelists are tuples and all edges use color0
    listc1 = [tuple(tuple(edge) for edge in elist) for elist in listc1]
    for edgelist in listc1:
        for n1,n2,c1,c2 in edgelist:
            assert((c1,c2) == (0,0))

    # get nodePermutations which to not change edgelist
    automorphisms = {}
    for edgelist in listc1:
        out = []
        for nodePermute in nodePermutations:
            new = permute_edges(edgelist, nodePermute, [0])
            if new == edgelist:
                out.append(nodePermute)
        automorphisms[edgelist] = out

    # look at all possible pairs (repeat allowed) from listc1
    # then look at all ways to combine these pairs:
    #   elist1 + permuted(elist2)
    # then find the minimum edgelist of all symmetry equivalent ways of writing this
    #
    # if we consider sorting the combined edges lists as:
    #   ( ... all color0 edges ... , ... all color1 edges ... )
    # then the color0 edges are already in the minimum
    # so we when trying to find the minimum edge list,
    # we only need to consider permutations that preserve list1
    runs = 0
    total = 0
    result = {}
    for i,elist1 in enumerate(listc1):
        list1_preserving_nodePermutations = automorphisms[elist1]
        out = set()
        for j in range(i,len(listc1)):
            elist2 = listc1[j]
            for nodePermute2 in nodePermutations:
                org = permute_edges(elist2, nodePermute2, [1])
                current = org
                # only need to try list1 perserving permutattions to find symmetry unique
                for nodePermute in list1_preserving_nodePermutations:
                    runs += 1
                    new = permute_edges(org, nodePermute, [0,1])
                    if new < current:
                        current = new
                out.add(current)
                #print(f"org:{org} --> min:{current}")
        out = list(out)
        out.sort(key=len)
        total += len(out)
        result[elist1] = out
    print(f"## inner loop runs = {runs}")

    return total, result, automorphisms


def get_ordered_cnode_edges_dict(edgelist):
    """
    build dict: cnode -> list of cnode
    such that (n2,c2) in dict[(n1,c1)] if and only if (n1,n2,c1,c2) in edgelist

    NOTE: to prevent double counting, the order of n1 and n2 does matter
    """
    cnode_edges = defaultdict(list)
    for n1,n2,c1,c2 in edgelist:
        cnode_edges[(n1,c1)].append((n2,c2))
    return cnode_edges

def get_cnode_edges_dict(edgelist):
    """
    list get_ordered_cnode_edges, but edge is listed under both cnodes
    (n2,c2) in dict[(n1,c1)] if (n1,n2,c1,c2) in edgelist or (n2,n1,c2,c1) in edgelist
    """
    cnode_edges = defaultdict(list)
    for n1,n2,c1,c2 in edgelist:
        cnode_edges[(n1,c1)].append((n2,c2))
        cnode_edges[(n2,c2)].append((n1,c1))
    return cnode_edges


def _check_monochrome_pm_recurse(need_nodes, cnode_edges, color):
    # Note: need_nodes will be modified
    if len(need_nodes) == 0:
        return True
    n1 = min(need_nodes)
    need_nodes.remove(n1)
    for idx,n2 in enumerate(need_nodes):
        if (n2,color) in cnode_edges[(n1,color)]:
            next_list = list(need_nodes)
            del next_list[idx]
            if _check_monochrome_pm_recurse(next_list, cnode_edges, color):
                return True
    return False

def has_monochrome_perfectmatch(cnode_edges, color, numNodes):
    need_nodes = list(range(numNodes))
    return _check_monochrome_pm_recurse(need_nodes, cnode_edges, color)


def check_c3_compatible(edgelist, numNodes, numColors):
    """
    edgelist : list of edge in format : tuple (n1,n1,c1,c2)

    check for:
    - at least one monochrome perfect matching of each color
    - no node can have all n-1 monochrome edges of a color incident on it
    """
    cnode_edges = get_cnode_edges_dict(edgelist)
    for c in range(numColors):
        if not has_monochrome_perfectmatch(cnode_edges, c, numNodes):
            return False

    for cnode1,cnodes2 in cnode_edges.items():
        n1,c1 = cnode1
        total_monochrome = 0
        for n2,c2 in cnodes2:
            if c1 == c2:
                total_monochrome += 1
        if total_monochrome >= numNodes - 1:
            return False

    return True

def generate_data(numNodes):
    print("===== ")
    print(f"numNodes = {numNodes}")
    out = brute_search1(numNodes)
    if numNodes <= 4:
        for i,edges in enumerate(out):
            print(f"#{i} : {edges}")
    print(f"all brute1 = {len(out)}")

    print("---- ")
    print("c3_compatible filtered")
    out1 = []
    for elist in out:
        if check_c3_compatible(elist, numNodes, 1):
            out1.append(elist)
    if numNodes <= 4:
        for i,edges in enumerate(out1):
            print(f"#{i} : {edges}")
    print(f"total1 = {len(out1)}")

    print("---- ")
    total2, out2, automorphisms1 = brute_search2(out1, numNodes)
    print(f"total2 = {total2}")

    # sanity check
    #   each entry from out1 should be a key in out2 and automorphisms
    #   so lengths should be the same
    assert(len(out1) == len(out2))
    assert(len(out1) == len(automorphisms1))

    info = {"total1":len(out), "total2":total2, "out2":out2, "automorphisms1":automorphisms1}

    fname = f"brute2_n{numNodes}.pickle"
    with open(fname, "wb") as f:
        pickle.dump(info, f)
    print("wrote data to", repr(fname))

# =========================================================================

generate_data(4)
generate_data(6)


