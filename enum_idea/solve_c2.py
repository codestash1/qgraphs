
"""
requires:
- ./Singular is symlink to Singular binary
  - and is built with prot_stderr.patch (or code below is modified to not turn on prot option)
- brute_enumerate_monocolor.py to have already precalculated data
- gen_base.py to have generated common base scripts
"""

import os
import sys
import itertools
import pickle
import subprocess
import time
import networkx as nx
from collections import defaultdict
sys.path.append("..")
from qgraphs import *
from qgraphs.draw import *

if len(sys.argv) < 2:
    print("Usage: python3 solve_c2.py numNodes")
    sys.exit(1)

numNodes = int(sys.argv[1])
numColors = 2
node0scale = True
useHelperConstraints = True
requireC3compatible = True


# requireC3compatible needs useHelperConstraints
assert(useHelperConstraints or not requireC3compatible)


# unbuffer output
import io
sys.stdout = io.TextIOWrapper(open(sys.stdout.fileno(), 'wb', 0), write_through=True)


with open(f"brute2_n{numNodes}.pickle", "rb") as f:
    info = pickle.load(f)

# info = {"total1":len(out), "total2":total2, "out2":out2, "automorphisms1":automorphisms1}
out2 = info["out2"]



def edge_name(edge):
    n1,c1 = edge.cnodes[0]
    n2,c2 = edge.cnodes[1]
    return "w{}{}{}{}".format(str(n1),str(n2),str(c1),str(c2))

def inv_edge_name(edge):
    return edge_name(edge).replace("w","v")

def clist_name(clist):
    return "p" + "".join(str(c) for c in clist)



def _check_scaling_all_edges_okay(nodepairs, numNodes):
    """
    checks if it is okay to scale all edge weights to 1
    (using monochrome constraint if necessary)

    assumes:
    1) nodepairs include at least one perfect matching
    2) all non-zero nodepairs for a particular color are included,
      so do not use this to check subsets of those edges.
    The second is important, because if there are multiple pm,
    the monochrome constraint is not useful.

    see groebner/scaling.txt for details,
    if there are no even cycles, there can be at most one pm,
      and so by assumption #1, here that means there is exactly one pm.

    it is therefore necessary and sufficient
    (see groebner/scaling.txt for details)
    to check:
      A) no component has an even cycle
        and
      B) no component has more than one cycle
    """

    G = nx.Graph(nodepairs)
    component_graphs = [G.subgraph(nodes).copy() for nodes in
                        nx.connected_components(G)]

    for subg in component_graphs:
        cbasis = nx.cycle_basis(subg)
        ncycles = len(cbasis)

        if ncycles == 0:
            continue

        # check: multiple cycles
        # are not possible to generically scale
        # Note: multiple pm will hit this or even cycle below
        if ncycles > 1:
            return False

        # check: even cycle
        # are not possible to generically scale
        assert(ncycles == 1)
        clen = len(cbasis[0])
        if clen%2 == 0:
            return False

    return True



def choose_color0_scaling_set(elist, numNodes):
    """
    elist must be list of all non-zero monochrome edges of color0

    this scaling is "all or nothing" to make automorphisms
    of the color0 subgraph play well with the groebner results

    Note: this could be loosened, when symmetry is broken
    example: could potentially scale all edges of some edge equivalence class
    """
    assert(all(c1 == 0 and c2 == 0 for n1,n2,c1,c2 in elist))

    nodepairs = [(n1,n2) for n1,n2,c1,c2 in elist]

    if _check_scaling_all_edges_okay(nodepairs, numNodes):
        return set(elist)
    return set()


def choose_scaling_set(elist, numNodes):
    """
    elist must be list of all non-zero monochrome edges of some common color
    """
    assert(all(c1 == c2 for n1,n2,c1,c2 in elist))
    assert(all(c1 == elist[0][3] for n1,n2,c1,c2 in elist))

    nodepairs = [(n1,n2) for n1,n2,c1,c2 in elist]

    # if possible, just scale all to 1
    if _check_scaling_all_edges_okay(nodepairs, numNodes):
        return set(elist)

    # check if there are multiple perfect matches
    edgelist = [Edge((n1,c1),(n2,c2)) for n1,n2,c1,c2 in elist]
    pminfo = calculate_perfect_matching_info(edgelist)
    assert(len(pminfo.components) == 1) # only one vertex coloring
    clist, pmlist = next(iter(pminfo.components.items()))
    assert( len(pmlist) > 0 ) # require at least one pm
    hasMultiplePM = (len(pmlist) > 1)

    # Note:
    # The following is non-optimal in that it may be possible to
    # scale more edges to 1.
    # However, not sure if more edges, or all edges from a node,
    # is better for groebner calculation.

    node_degree = [0]*numNodes
    for n1,n2 in nodepairs:
        node_degree[n1] += 1
        node_degree[n2] += 1

    max_deg = max(node_degree)

    out = set()
    if hasMultiplePM:
        # for now, default to scaling all edges on highest degree node
        node = node_degree.index(max_deg)
        for e in elist:
            n1,n2,c1,c2 = e
            if n1 == node or n2 == node:
                out.add(e)
    else:
        # case: simple pm

        # just greedily add star graph subgraphs from the edge list
        #   building a tree or disconnected trees
        # This is done by, after each greedy addition, reducing to
        #   the set of edges that connect to currently unused nodes.
        # The result could be a tree with a perfect matching,
        #   so is only appropriate in the single pm case.
        eset = set(elist)
        unused_nodes = set(range(numNodes))
        while max_deg > 0:
            node = node_degree.index(max_deg)
            for e in eset:
                n1,n2,c1,c2 = e
                if n1 == node or n2 == node:
                    unused_nodes.discard(n1)
                    unused_nodes.discard(n2)
                    out.add(e)

            node_degree = [0]*numNodes
            new_eset = ()
            for e in eset:
                n1,n2,c1,c2 = e
                if n1 in unused_nodes or n2 in unused_nodes:
                    node_degree[n1] += 1
                    node_degree[n2] += 1
                    new_eset.add(e)
            eset = new_eset
            max_deg = max(node_degree)

    return out



temp_run_fname = f"temp_run_n{numNodes}c{numColors}.s"
skip = False
for i,elist1 in enumerate(out2):
    scale_set1 = choose_color0_scaling_set(elist1, numNodes)
    for j,elist2 in enumerate(out2[elist1]):
        edgelist = [Edge((n1,c1),(n2,c2)) for n1,n2,c1,c2 in elist1]
        edgelist.extend(Edge((n1,c1),(n2,c2)) for n1,n2,c1,c2 in elist2)

        scale_set = scale_set1.union(choose_scaling_set(elist2, numNodes))

        outfname = f"n{numNodes}c{numColors}_{i}_{j}.out"
        if os.path.exists(outfname):
            skip = True
            continue
        if skip:
            print(f"... skip to #{i}_{j}")
            skip=False
        start = time.time()
        print(edgelist)

        eset = set(edgelist)
        with open(temp_run_fname,"w") as f:
            f.write('// load constants\n')
            f.write(f'< "base_n{numNodes}.s";\n')
            f.write("option(prot);\n") # would be nice if this went to stderr by default
            extra_constraints = []
            for n1 in range(numNodes):
                for n2 in range(n1+1,numNodes):
                    for c in range(numColors):
                        edge = Edge((n1,c),(n2,c))
                        if edge in eset:
                            if (n1,n2,c,c) in scale_set:
                                extra_constraints.append("{} - 1".format(
                                    edge_name(edge)))
                            else:
                                extra_constraints.append("{}*{} - 1".format(
                                    edge_name(edge), inv_edge_name(edge)))
                        else:
                            extra_constraints.append("{}".format(
                                edge_name(edge)))
            if useHelperConstraints:
                if requireC3compatible:
                    f.write("ideal I = Ic2, helpC2, helpC23,\n")
                else:
                    f.write("ideal I = Ic2, helpC2,\n")
            else:
                f.write("ideal I = Ic2,\n")
            f.write("  " + ",\n  ".join(extra_constraints) + ";\n")
            f.write("ideal I2 = slimgb(I);\n")
            f.write("I2;\n")

        out = subprocess.run(["./Singular","-q","--cntrlc=q"], check=True,
                            stdin=open(temp_run_fname,"rb"),
                            stderr=sys.stderr,
                            stdout=subprocess.PIPE)
        #print("  out =",out)
        runtime = time.time() - start
        has_solution = not (b"I2[1]=1\n" in out.stdout)

        print(f"  result #{i}_{j} : {has_solution} : time={runtime}")
        with open(outfname, "wb") as f:
            f.write(out.stdout)

        if 0 and has_solution:
            draw_graph(edgelist)
            plt.show()


